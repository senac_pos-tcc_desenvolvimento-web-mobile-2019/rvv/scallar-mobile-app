<div align="center">
<img alt="Scallar" src="src/assets/scallar_marca@3x.png">
<h3>Scallar</h3>
<h4 align="center">expreriência mobile do sistema Scallar</h4>
</div>
<div style="margin: 20px;">
  <div style="max-width: 900px;">
    <h2>Descrição</h2>
    <p>App mobile desenvolvido em react native objetivando trazer da forma mais completa
      e confortavel as funcionalidades do sistema web para celulares android, focando na experiencia dos usuários da scallar</p>
    <h2>Instalação</h2>
    <h3>Iniciando projeto</h3>
    <p>Escolha a pasta que deva conter o projeto e baixe pelo comando.</p>
    <code>git clone git@gitlab.com:senac_pos-tcc_desenvolvimento-web-mobile-2019/rvv/scallar-mobile-app.git</code>
    <h4>Para rodar pelo react native android.</h4>
    <p>Neste caso é necessário possuir o react-native, java 8 instalados</p>
    <code>cd scallar-mobile-app</code><br/>
    <code>yarn</code><br/>
    <code>yarn android</code><br/>
    <p>O app será instalado automaticamente no emulador ou no celular conectado ao computador</p>
    <h4>Executar pelo apk</h4>
    <p>O arquivo apk pode ser encontrado no caminho.</p>
    <code>android -> app -> build -> outputs -> apk  -> release -> app-release.apk</code>
    <p>O app pode ser instalado arrastando o apk para o emulador ou baixando o arquivo para o celular.</p>
    <h2>Utilização</h2>
    <p>Inicialmente o usuário deve se cadastrar clicando no botão na parte inferior da tela de login.</p>
    <p>Apos logar na aplicação o usuario é direcionando para a tela que possui os dados da empresa a qual estará vazia se
      não houver cadastro da mesma, neste caso deve ser cadastrar a empresa pelo caminho.</p>
    <code>clicando no hamburger button na parte superior esquerda -> Dados da Empresa -> Cadastro Empresa</code>
    <h2>Estrutura</h2>
    <h4>Esta aplicação é dividia em dois projetos.</h4>
    <p>Scallar_Mobile_App em typescript com react native</p>
    <p>Scallar_Laravel_API em php com laravel</p>
    <h4>O projeto Scallar_Laravel_API pode ser acessado pelo endereço:</h4>
    <p>https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/rvv/scallar_laravel_api</p>
    <h2>Download apk do projeto</h2>
    <p>https://gitlab.com/senac_pos-tcc_desenvolvimento-web-mobile-2019/rvv/scallar-mobile-app/-/blob/master/android/app/build/outputs/apk/release/app-release.apk</p>
  </div>
</div>
