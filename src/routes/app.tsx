import React from 'react';
import {LogBox} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Header from '../components/Header';

import DrawerRouter from './drawer';

const App = createStackNavigator();

LogBox.ignoreAllLogs(true);

const AppRoute: React.FC = () => (
  <App.Navigator
    screenOptions={{
      headerBackTitleVisible: false,
      headerTitle: (props) => <Header {...props} />,
      headerStyle: {
        backgroundColor: '#fff',
        elevation: 0,
        shadowColor: 'transparent',
      },
    }}>
    <App.Screen name="NavScreen" component={DrawerRouter} />
  </App.Navigator>
);

export default AppRoute;
