// TODO: fazer autenticação e tela de loading
import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import {useAuth} from '../hooks/Auth';
import AuthRouter from './auth';
import AppRouter from './app';
import DrawerRouter from './drawer';

const Routes: React.FC = () => {
  const {user, loading} = useAuth();
  if (loading) {
    return (
      <View style={activityViewStyle.container}>
        <ActivityIndicator size="large" color="#12967c" />
      </View>
    );
  }
  return user ? (
    <AppRouter>
      <DrawerRouter />
    </AppRouter>
  ) : (
    <AuthRouter />
  );
};

const activityViewStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Routes;
