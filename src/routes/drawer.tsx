import * as React from 'react';
import {StyleSheet} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerMenu from '../components/DrawerMenu';
import Dashboard from '../pages/Dashboard';
import User from '../pages/Profile';
import CompanyManagement from '../pages/Company/Management';
import CompanyRevenues from '../pages/Company/Revenues';
import CompanyListRevenues from '../pages/Company/Revenues/List';
import CompanyCustomers from '../pages/Company/Customers';
import CompanyListCustomers from '../pages/Company/Customers/List';
import CompanyMarketingFunds from '../pages/Company/MarketingFunds';
import CompanyListMarketingFunds from '../pages/Company/MarketingFunds/List';
import CompanyMarketingActions from '../pages/Company/MarketingActions';
import CompanyListMarketingActions from '../pages/Company/MarketingActions/List';
import CompanyInvestments from '../pages/Company/Investments';
import CompanyListInvestments from '../pages/Company/Investments/List';
import CompanyExpenses from '../pages/Company/Expenses';
import CompanyListExpenses from '../pages/Company/Expenses/List';
import CompanyNps from '../pages/Company/Nps';
import CompanyListNps from '../pages/Company/Nps/List';
import AverageTicket from '../pages/Measure/AverageTicket';
import CAC from '../pages/Measure/CAC';
import LTV from '../pages/Measure/LTV';
import LTV_CAC from '../pages/Measure/LTV_CAC';
import PayBack from '../pages/Measure/PayBack';
import ROI from '../pages/Measure/ROI';
import ROMI from '../pages/Measure/ROMI';
import NPS from '../pages/Measure/NPS';

const Drawer = createDrawerNavigator();

export default function navRoutes() {
  return (
    <Drawer.Navigator
      initialRouteName="Dashboard"
      drawerStyle={DrawerStyle.fullwidth}
      screenOptions={{gestureEnabled: false}}
      drawerContent={() => <DrawerMenu />}>
      <Drawer.Screen name="Dashboard" component={Dashboard} />
      <Drawer.Screen name="User" component={User} />
      {/*Company Routes*/}
      <Drawer.Screen name="CompanyManagement" component={CompanyManagement} />
      <Drawer.Screen name="CompanyRevenues" component={CompanyRevenues} />
      <Drawer.Screen
        name="CompanyListRevenues"
        component={CompanyListRevenues}
      />
      <Drawer.Screen name="CompanyCustomers" component={CompanyCustomers} />
      <Drawer.Screen
        name="CompanyListCustomers"
        component={CompanyListCustomers}
      />
      <Drawer.Screen
        name="CompanyMarketingFunds"
        component={CompanyMarketingFunds}
      />
      <Drawer.Screen
        name="CompanyListMarketingFunds"
        component={CompanyListMarketingFunds}
      />
      <Drawer.Screen
        name="CompanyMarketingActions"
        component={CompanyMarketingActions}
      />
      <Drawer.Screen
        name="CompanyListMarketingActions"
        component={CompanyListMarketingActions}
      />
      <Drawer.Screen name="CompanyInvestments" component={CompanyInvestments} />
      <Drawer.Screen
        name="CompanyListInvestments"
        component={CompanyListInvestments}
      />
      <Drawer.Screen name="CompanyExpenses" component={CompanyExpenses} />
      <Drawer.Screen
        name="CompanyListExpenses"
        component={CompanyListExpenses}
      />
      <Drawer.Screen name="CompanyNps" component={CompanyNps} />
      <Drawer.Screen name="CompanyListNps" component={CompanyListNps} />
      {/*Measure Routes*/}
      <Drawer.Screen name="AverageTicket" component={AverageTicket} />
      <Drawer.Screen name="CAC" component={CAC} />
      <Drawer.Screen name="LTV" component={LTV} />
      <Drawer.Screen name="LTV_CAC" component={LTV_CAC} />
      <Drawer.Screen name="PayBack" component={PayBack} />
      <Drawer.Screen name="ROI" component={ROI} />
      <Drawer.Screen name="ROMI" component={ROMI} />
      <Drawer.Screen name="NPS" component={NPS} />
      {/*Others Routes*/}
    </Drawer.Navigator>
  );
}

const DrawerStyle = StyleSheet.create({
  fullwidth: {
    width: '100%',
  },
});
