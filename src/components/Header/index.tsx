import React, {useCallback} from 'react';
import {View} from 'react-native';
import {useNavigation, DrawerActions} from '@react-navigation/native';
import {useAuth} from '../../hooks/Auth';
import {
  Container,
  Hamburger,
  Lineup,
  GreenLine,
  Linedown,
  LogoButton,
  Logo,
  HeaderTitle,
  GreenText,
  UserContainer,
  UserName,
} from './styles';
import Icon from 'react-native-vector-icons/Feather';
import Logoimg from '../../assets/scallar_marca.png';

const Header: React.FC = () => {
  const navigation = useNavigation();
  const {user} = useAuth();

  const toggleDrawer = useCallback(() => {
    navigation.dispatch(DrawerActions.toggleDrawer());
  }, [navigation]);

  const GoToDashboardHandle = useCallback(() => {
    navigation.dispatch(DrawerActions.closeDrawer());
    navigation.navigate('Dashboard');
  }, [navigation]);

  return (
    <Container>
      <Hamburger onPress={toggleDrawer}>
        <View>
          <Lineup />
          <GreenLine />
          <Linedown />
        </View>
      </Hamburger>
      <LogoButton onPress={GoToDashboardHandle}>
        <Logo source={Logoimg} />
        <HeaderTitle>
          Sca<GreenText>l</GreenText>lar
        </HeaderTitle>
      </LogoButton>
      <UserContainer onPress={() => navigation.navigate('User')}>
        <Icon name="user" size={24} color="#12967c" />
        <UserName>{user.name}</UserName>
      </UserContainer>
    </Container>
  );
};

export default Header;
