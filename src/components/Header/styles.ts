import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Hamburger = styled.TouchableOpacity``;

export const Lineup = styled.View`
  width: 42px;
  border-bottom-color: #404040;
  border-bottom-width: 4px;
  margin-bottom: 6px;
`;

export const GreenLine = styled.View`
  width: 42px;
  border-bottom-color: #12967c;
  border-bottom-width: 4px;
  margin-bottom: 6px;
`;

export const Linedown = styled.View`
  width: 42px;
  border-bottom-color: #404040;
  border-bottom-width: 4px;
  margin-bottom: 6px;
`;

export const LogoButton = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Logo = styled.Image`
  width: 42px;
  height: 42px;
`;

export const HeaderTitle = styled.Text`
  font-size: 24px;
  font-weight: bold;
  color: #404040;
`;

export const GreenText = styled.Text`
  font-size: 24px;
  font-weight: bold;
  color: #12967c;
`;

export const UserContainer = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
`;

export const UserName = styled.Text`
  font-size: 16px;
`;
