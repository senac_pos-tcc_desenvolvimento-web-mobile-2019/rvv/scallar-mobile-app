import React, {useState, useCallback} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Container, GoBackButton, LinkMenu, LinkMenuText, Link} from './styles';
import Company from './Company';
import Measure from './Measure';
import Icon from 'react-native-vector-icons/Feather';

const DrawerMenu: React.FC = () => {
  const navigation = useNavigation();
  const [drawerToggle, setDrawerToggle] = useState(false);
  const [companyToggle, setCompanyToggle] = useState(false);
  const [measureToggle, setMeasureToggle] = useState(false);

  const goBackHandler = useCallback(() => {
    setDrawerToggle(false);
    setCompanyToggle(false);
    setMeasureToggle(false);
  }, []);
  return (
    <Container>
      <GoBackButton onPress={goBackHandler} enable={drawerToggle}>
        <Icon name="chevron-left" size={24} color="#12967c" />
        <LinkMenuText>Voltar</LinkMenuText>
      </GoBackButton>
      <LinkMenu
        disable={drawerToggle}
        onPress={() => {
          setDrawerToggle(true);
          setCompanyToggle(true);
        }}>
        <LinkMenuText>Dados da Empresa</LinkMenuText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </LinkMenu>
      {companyToggle && <Company />}
      <LinkMenu
        disable={drawerToggle}
        onPress={() => {
          setDrawerToggle(true);
          setMeasureToggle(true);
        }}>
        <LinkMenuText>Métricas</LinkMenuText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </LinkMenu>
      {measureToggle && <Measure />}
      <Link
        disable={drawerToggle}
        onPress={() => {
          navigation.navigate('NPS');
        }}>
        <LinkMenuText>NPS</LinkMenuText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      {/* <LinkMenu disable={drawerToggle} onPress={() => {}}>
        <LinkMenuText>Integrações (em construção)</LinkMenuText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </LinkMenu>
      <LinkMenu disable={drawerToggle} onPress={() => {}}>
        <LinkMenuText>Relatórios (em construção)</LinkMenuText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </LinkMenu>
      <LinkMenu disable={drawerToggle} onPress={() => {}}>
        <LinkMenuText>Pagamento (em construção)</LinkMenuText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </LinkMenu>
      <LinkMenu disable={drawerToggle} onPress={() => {}}>
        <LinkMenuText>Relatar um Problema (em construção)</LinkMenuText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </LinkMenu> */}
    </Container>
  );
};

export default DrawerMenu;
