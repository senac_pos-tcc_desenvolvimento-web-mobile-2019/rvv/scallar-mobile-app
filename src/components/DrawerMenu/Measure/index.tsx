import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {Container, Link, LinkText} from './styles';
import Icon from 'react-native-vector-icons/Feather';

const Measure: React.FC = () => {
  const navigation = useNavigation();

  return (
    <Container showsVerticalScrollIndicator={false}>
      <Link
        onPress={() => {
          navigation.navigate('AverageTicket');
        }}>
        <LinkText>Ticket Médio</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('CAC');
        }}>
        <LinkText>CAC</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('LTV');
        }}>
        <LinkText>LTV</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('LTV_CAC');
        }}>
        <LinkText>LTV:CAC</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('PayBack');
        }}>
        <LinkText>PayBack</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('ROI');
        }}>
        <LinkText>ROI</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('ROMI');
        }}>
        <LinkText>ROMI</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
    </Container>
  );
};

export default Measure;
