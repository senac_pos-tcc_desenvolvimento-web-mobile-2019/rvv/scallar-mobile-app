import styled from 'styled-components/native';

export const Container = styled.ScrollView``;

export const Link = styled.TouchableOpacity`
  background-color: #fff;
  margin-bottom: 16px;
  padding: 12px 32px;
  justify-content: space-between;
  flex-direction: row;
  border-radius: 15px;
`;

export const LinkText = styled.Text`
  color: #404040;
  font-size: 20px;
  font-weight: bold;
`;
