import styled, {css} from 'styled-components/native';

interface drawerMenu {
  disable: boolean;
}

interface GoBackButtonProps {
  enable: boolean;
}

export const Container = styled.View`
  height: 100%;
  padding: 36px;
  background-color: #12967c;
`;

export const GoBackButton = styled.TouchableOpacity<GoBackButtonProps>`
  background-color: #fff;
  margin-bottom: 16px;
  padding: 16px 32px;
  justify-content: space-between;
  flex-direction: row;
  border-radius: 15px;
  ${(props) =>
    !props.enable &&
    css`
      display: none;
    `}
  ${(props) =>
    props.enable &&
    css`
      display: flex;
    `}
`;

export const LinkMenu = styled.TouchableOpacity<drawerMenu>`
  background-color: #fff;
  margin-bottom: 16px;
  padding: 16px 32px;
  justify-content: space-between;
  flex-direction: row;
  border-radius: 15px;
  ${(props) =>
    props.disable &&
    css`
      display: none;
    `}
  ${(props) =>
    !props.disable &&
    css`
      display: flex;
    `}
`;

export const LinkMenuText = styled.Text`
  color: #404040;
  font-size: 20px;
  font-weight: bold;
`;

export const Link = styled.TouchableOpacity<drawerMenu>`
  background-color: #fff;
  margin-bottom: 16px;
  padding: 12px 32px;
  justify-content: space-between;
  flex-direction: row;
  border-radius: 15px;
  ${(props) =>
    props.disable &&
    css`
      display: none;
    `}
  ${(props) =>
    !props.disable &&
    css`
      display: flex;
    `}
`;
