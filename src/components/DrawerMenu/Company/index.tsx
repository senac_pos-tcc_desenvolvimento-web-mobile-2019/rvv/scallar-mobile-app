import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {Container, Link, LinkText} from './styles';
import Icon from 'react-native-vector-icons/Feather';

const Company: React.FC = () => {
  const navigation = useNavigation();

  return (
    <Container showsVerticalScrollIndicator={false}>
      <Link
        onPress={() => {
          navigation.navigate('CompanyManagement');
        }}>
        <LinkText>Cadastro de Empresa</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('CompanyRevenues');
        }}>
        <LinkText>Faturamentos</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('CompanyCustomers');
        }}>
        <LinkText>Clientes</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('CompanyMarketingFunds');
        }}>
        <LinkText>Verba de Marketing</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('CompanyMarketingActions');
        }}>
        <LinkText>Ações de Marketing</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('CompanyInvestments');
        }}>
        <LinkText>Investimentos</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('CompanyExpenses');
        }}>
        <LinkText>Despesas com Vendas</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
      <Link
        onPress={() => {
          navigation.navigate('CompanyNps');
        }}>
        <LinkText>Dados do NPS</LinkText>
        <Icon name="chevron-right" size={24} color="#12967c" />
      </Link>
    </Container>
  );
};

export default Company;
