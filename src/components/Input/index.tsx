import React, {
  useState,
  useCallback,
  useEffect,
  useRef,
  useImperativeHandle,
  forwardRef,
} from 'react';
import {TextInputProps} from 'react-native';
import {useField} from '@unform/core';
import Icon from 'react-native-vector-icons/Feather';
import {Container, TextInput} from './styles';

interface InputProps extends TextInputProps {
  name: string;
  icon?: string;
  halfSize?: boolean;
}

interface InputReference {
  value: string;
}

interface InputRef {
  focus(): void;
}

const Input: React.ForwardRefRenderFunction<InputRef, InputProps> = (
  {name, icon, halfSize, editable, ...rest},
  ref,
) => {
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);
  const inputElementRef = useRef<any>(null);
  const {registerField, defaultValue, fieldName, error} = useField(name);
  const inputRef = useRef<InputReference>({value: defaultValue});

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);
    setIsFilled(!!inputRef.current.value);
  }, []);

  useImperativeHandle(ref, () => ({
    focus() {
      inputElementRef.current.focus();
    },
  }));

  useEffect(() => {
    if (defaultValue) {
      inputRef.current.value = defaultValue;
      inputElementRef.current.setNativeProps({text: defaultValue});
    }
  }, [defaultValue]);

  useEffect(() => {
    registerField<string>({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
      setValue(_, value) {
        inputRef.current.value = value;
        inputElementRef.current.setNativeProps({text: value});
      },
      clearValue() {
        inputRef.current.value = '';
        inputElementRef.current.clear();
      },
    });
  }, [fieldName, registerField]);

  return (
    <Container
      focused={isFocused}
      errored={!!error}
      filled={isFilled}
      halfSize={halfSize}
      editable={editable}>
      {icon && (
        <Icon
          name={icon}
          size={24}
          color={
            error ? '#c53030' : isFocused || isFilled ? '#12967c' : '#404040'
          }
        />
      )}
      <TextInput
        ref={inputElementRef}
        onChangeText={(value) => {
          inputRef.current.value = value;
        }}
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
        defaultValue={defaultValue}
        placeholderTextColor="#222"
        editable={editable}
        {...rest}
      />
    </Container>
  );
};

export default forwardRef(Input);
