import styled, {css} from 'styled-components/native';

interface ContainerProps {
  focused: boolean;
  filled: boolean;
  errored: boolean;
  halfSize?: boolean;
  editable?: boolean;
}

export const Container = styled.View<ContainerProps>`
  flex-direction: row;
  align-items: center;
  width: 300px;
  height: 64px;
  padding-left: 16px;
  padding-right: 32px;
  background-color: #fff;
  border: 2px solid #404040;
  border-radius: 15px;
  margin-bottom: 8px;
  ${(props) =>
    props.halfSize &&
    css`
      width: 150px;
    `}
  ${(props) =>
    props.errored &&
    css`
      border-color: #c53030;
    `}
  ${(props) =>
    (props.focused || props.filled) &&
    css`
      border-color: #12967c;
    `}
    ${(props) =>
    props.editable === false &&
    css`
      opacity: 0.5;
    `}
`;

export const TextInput = styled.TextInput`
  margin-left: 8px;
  font-size: 20px;
`;
