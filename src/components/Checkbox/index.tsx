import React, {useState, useEffect, useRef} from 'react';
import CheckBox, {CheckBoxProps} from '@react-native-community/checkbox';
import {useField} from '@unform/core';
import {Container, Label} from './styles';

interface CheckBoxInputProps extends CheckBoxProps {
  name: string;
}

interface CheckReference {
  value?: boolean;
}

const Checkbox: React.FC<CheckBoxInputProps> = ({name, children, ...rest}) => {
  const {registerField, defaultValue = false, fieldName} = useField(name);
  const [isSelected, setIsSelected] = useState(false);
  const checkRef = useRef<CheckReference>({value: defaultValue});

  useEffect(() => {
    registerField<boolean>({
      name: fieldName,
      ref: checkRef.current,
      path: 'value',
      setValue(_, value) {
        checkRef.current.value = value;
      },
      clearValue() {
        checkRef.current.value = false;
      },
    });
  }, [fieldName, registerField]);

  return (
    <Container>
      <CheckBox
        value={isSelected}
        onValueChange={(value) => {
          setIsSelected(value);
          checkRef.current.value = value;
        }}
        {...rest}
      />
      <Label>{children}</Label>
    </Container>
  );
};

export default Checkbox;
