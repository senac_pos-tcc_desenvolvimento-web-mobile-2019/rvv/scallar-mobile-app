import styled from 'styled-components/native';
import CheckBoxProps from '@react-native-community/checkbox';

export const Container = styled.View`
  flex-direction: row;
  padding-left: 5px;
  width: 300px;
  margin-bottom: 4px;
  align-items: center;
`;

export const CheckBox = styled(CheckBoxProps)``;

export const Label = styled.Text`
  padding-right: 30px;
  text-align: center;
  font-size: 18px;
`;
