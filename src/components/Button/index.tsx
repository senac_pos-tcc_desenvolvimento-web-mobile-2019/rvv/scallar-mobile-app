import React from 'react';
import {RectButtonProperties} from 'react-native-gesture-handler';
import {Container, ButtonText} from './styles';

interface ButtonProps extends RectButtonProperties {
  children: string;
  disabled?: boolean;
  color?: string;
  halfSize?: boolean;
}

const Button: React.FC<ButtonProps> = ({
  children,
  disabled,
  halfSize,
  ...rest
}) => {
  return (
    <Container
      halfSize={halfSize}
      disabled={disabled}
      enabled={!disabled}
      {...rest}>
      <ButtonText>{children}</ButtonText>
    </Container>
  );
};

export default Button;
