import styled, {css} from 'styled-components/native';
import {RectButton, RectButtonProperties} from 'react-native-gesture-handler';

interface ScallarButtonProps extends RectButtonProperties {
  disabled?: boolean;
  color?: string;
  halfSize?: boolean;
}

export const Container = styled(RectButton)<ScallarButtonProps>`
  width: 300px;
  height: 48px;
  background-color: #12967c;
  border-radius: 15px;
  justify-content: center;
  align-items: center;
  margin: 8px 4px;
  ${({disabled}) =>
    disabled &&
    css`
      background-color: #a0d5ca;
    `};
  ${({color}) =>
    color &&
    css`
      background-color: ${color} !important;
    `};
  ${({halfSize}) =>
    halfSize &&
    css`
      width: 150px;
      height: 52px;
    `}
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 20px;
  font-weight: bold;
`;
