import styled, {css} from 'styled-components/native';

interface ContainerProps {
  halfSize?: boolean;
}

export const Container = styled.View<ContainerProps>`
  overflow: hidden;
  flex-direction: row;
  align-items: center;
  width: 300px;
  height: 64px;
  padding-left: 19px;
  background-color: #fff;
  border: 2px solid #404040;
  border-radius: 15px;
  margin-bottom: 8px;
  ${(props) =>
    props.halfSize &&
    css`
      width: 150px;
    `}
`;
