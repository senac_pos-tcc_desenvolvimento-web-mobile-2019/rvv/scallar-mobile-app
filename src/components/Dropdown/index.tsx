import React, {useRef, useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {useField} from '@unform/core';
import {Container} from './styles';

export interface DropdownDataProps {
  label: string;
  value: number;
}

interface InputReference {
  value: number;
}

interface PickerProps {
  name: string;
  data: Array<DropdownDataProps> | undefined;
  halfSize?: boolean;
}

const Dropdown: React.FC<PickerProps> = ({name, data, halfSize}) => {
  const {registerField, defaultValue = 0, fieldName} = useField(name);
  const [selected, setSelected] = useState(0);
  const pickerRef = useRef<InputReference>({value: defaultValue});

  useEffect(() => {
    if (defaultValue) {
      pickerRef.current.value = defaultValue;
    }
  }, [defaultValue]);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: pickerRef.current,
      path: 'value',
      setValue(_, value: number) {
        setSelected(value);
        pickerRef.current.value = value;
      },
      clearValue() {
        pickerRef.current.value = 0;
      },
    });
  }, [fieldName, registerField]);

  return (
    <Container halfSize={halfSize}>
      <Picker
        selectedValue={selected || defaultValue}
        style={PickerStyle.picker}
        onValueChange={(itemValue) => {
          setSelected(Number(itemValue));
          pickerRef.current.value = Number(itemValue);
        }}>
        {data ? (
          data.map((dt) => {
            return (
              <Picker.Item key={dt.value} label={dt.label} value={dt.value} />
            );
          })
        ) : (
          <Picker.Item key={0} label={'Carregando'} value={0} />
        )}
      </Picker>
    </Container>
  );
};

const PickerStyle = StyleSheet.create({
  picker: {
    width: 270,
    height: 64,
  },
});

export default Dropdown;
