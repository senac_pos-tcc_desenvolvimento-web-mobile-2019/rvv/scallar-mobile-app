import React from 'react';
import {RectButtonProperties} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import {Container} from './styles';

interface ButtonProps extends RectButtonProperties {
  icon: string;
  disabled?: boolean;
}

const Button: React.FC<ButtonProps> = ({icon, disabled, ...rest}) => {
  return (
    <Container disabled={disabled} enabled={!disabled} {...rest}>
      <Icon name={icon} size={40} color="#fff" />
    </Container>
  );
};

export default Button;
