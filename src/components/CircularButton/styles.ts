import styled, {css} from 'styled-components/native';
import {RectButton, RectButtonProperties} from 'react-native-gesture-handler';

interface ScallarButtonProps extends RectButtonProperties {
  disabled?: boolean;
}

export const Container = styled(RectButton)<ScallarButtonProps>`
  position: absolute;
  bottom: 10px;
  right: 10px;
  width: 64px;
  height: 64px;
  background-color: #12967c;
  border-radius: 32px;
  justify-content: center;
  align-items: center;
  margin: 8px;
  ${(props) =>
    props.disabled &&
    css`
      background-color: #a0d5ca;
    `};
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 20px;
  font-weight: bold;
`;
