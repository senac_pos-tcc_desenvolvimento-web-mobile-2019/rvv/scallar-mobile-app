import React, {
  useState,
  useCallback,
  createContext,
  useContext,
  useEffect,
} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../services/api';
import {RegisterFormData} from '../pages/Company/Management/dtos';

interface SigninCredentials {
  email: string;
  senha: string;
}

// removido email e avatar de User
interface User {
  id: number;
  name: string;
  empresa?: RegisterFormData;
}

interface AuthState {
  token?: string;
  user: User;
}

interface AuthContextData {
  user: User;
  loading: boolean;
  signin(credential: SigninCredentials): Promise<void>;
  signout(): void;
  updateUser(user: User): void;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({children}) => {
  const [data, setData] = useState<AuthState>({} as AuthState);
  const [loading, setLoading] = useState(true);

  // removido o token de todos setData<AuthState>

  useEffect(() => {
    async function loadStorage(): Promise<void> {
      //const token = await AsyncStorage.getItem('@scallar:token');
      try {
        await api.get('/listarEmpresaAPI');
      } catch (err) {
        await AsyncStorage.removeItem('@scallar:userId');
        setData({} as AuthState);
      }
      const user = await AsyncStorage.getItem('@scallar:userId');
      // removido token do if
      if (user) {
        setData({user: JSON.parse(user)});
      }
      setLoading(false);
    }
    loadStorage();
  }, []);

  const signin = useCallback(async ({email, senha}) => {
    const response = await api.post('/loginUsuarioAPI', {
      email,
      password: senha,
    });
    if (response.data[0] === 'E') {
      throw new Error('E-mail ou senha invalidos');
    }
    const user: User = {
      id: response.data[0],
      name: response.data[1],
    };
    const resCompany = await api.get('/listarEmpresaAPI');
    if (resCompany.data.data[0]) {
      user.empresa = resCompany.data.data[0];
    }
    //const { user} = response.data;
    //await AsyncStorage.setItem('@scallar:token', token);
    await AsyncStorage.setItem('@scallar:userId', JSON.stringify(user));
    //api.defaults.headers.authorization = `Bearer ${token}`;
    setData({user});
  }, []);

  const signout = useCallback(async () => {
    //await AsyncStorage.removeItem('@scallar:token');
    await AsyncStorage.removeItem('@scallar:userId');
    setData({} as AuthState);
  }, []);

  const updateUser = useCallback(
    async (user: User) => {
      await AsyncStorage.setItem('@scallar:userId', JSON.stringify(user));
      setData({
        token: data.token,
        user,
      });
    },
    [data.token],
  );

  return (
    <AuthContext.Provider
      value={{user: data.user, signin, signout, updateUser, loading}}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): AuthContextData {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
}

export {AuthProvider, useAuth};
