export const TermoDeUso =
  '   ACORDO SOBRE OS TERMOS DE USO E PRIVACIDADE DO SITE E PLATAFORMA SCALLAR  ' +
  '\n\n' +
  '   ATUALIZADO EM 13 DE NOVEMBRO DE 2019  ' +
  '\n\n' +
  '   Bem-vindo à Plataforma Scallar, de propriedade da Scallar (a seguir denominada “Licenciante”). Estes temos e condições de uso (os “Termos de Uso”) se aplicam à Plataforma Scallar localizada em https://app.scallar.com.br, e a todos os Sites associados vinculados a http://scallar.com.br pela Licenciante.  ' +
  '\n' +
  '     ' +
  '   A Plataforma Scallar é propriedade da Scallar Ltda. (“Licenciante”) e a Licenciante opera a Plataforma para fornecer acesso on-line aos produtos, serviços e oportunidades oferecidas. O uso de produtos e serviços da Licenciante é regido por termos e condições separados fornecidos na Plataforma ou por outros meios disponíveis.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   IMPORTANTE – LEIA COM ATENÇÃO: \n\n' +
  '     ' +
  '   AO CLICAR NO BOTÃO “CONCORDO”, “ACEITO” OU “CONCLUÍDO”, VOCÊ RECONHECE QUE ESTÁ AUTORIZADO A ADERIR A ESTES TERMOS DE USO E CONTRATO DE SERVIÇO DECORRENTE (“ACORDO”) E SE VOCÊ ESTÁ ACEITANDO EM NOME DO SEU EMPREGADOR OU OUTRA ENTIDADE (“CLIENTE”), VOCÊ REPRESENTA E GARANTE QUE TEM LEGITIMIDADE E PODERES PARA REPRESENTAR TAL ENTIDADE E CONCORDAR COM TODOS OS TERMOS NO QUE SE REFERE AO USO DOS PRODUTOS E SERVIÇOS OFERECIDOS PELA LICENCIANTE PELO CLIENTE.  ' +
  '\n' +
  '\n     ' +
  '   SE VOCÊ NÃO CONCORDAR COM TODOS ESTES TERMOS DE USO, POLÍTICA DE PRIVACIDADE E DEMAIS DOCUMENTOS OU NÃO ESTÁ AUTORIZADO REPRESENTAR O CLIENTE NESTES TERMOS DE USO E CONTRATO, NÃO CLIQUE NO BOTÃO “CONCORDO”, “ACEITO” OU “CONCLUÍDO” OU CLIQUE NO BOTÃO “NÃO ACEITO” OU FECHE O JANELA PARA PARAR A CRIAÇÃO DA CONTA NA PLATAFORMA SCALLAR.  ' +
  '\n' +
  ' \n    ' +
  '   A Licenciante reserva-se o direito, a seu exclusivo critério, de fazer alterações na Plataforma Scallar e nestes Termos de Uso e Política de Privacidade a qualquer momento. Cada vez que você usar a Plataforma Scallar, você deve rever os Termos de Uso e Política de Privacidades atuais que se aplicam às suas transações e uso desta Plataforma. O uso continuado da Plataforma Scallar após a publicação de alterações significa que você aceita e concorda com as alterações. Se você está insatisfeito com a Plataforma Scallar, seu conteúdo ou Termos de Uso e Política de Privacidade, você concorda que sua única e exclusiva opção é interromper o uso do Site.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Licenciante notificará todos os usuários da Plataforma Scallar sempre que houver alterações nos Termos de Uso e Política de Privacidade, assim o usuário poderá decidir se aceita e concorda com as alterações ou se interrompe o uso da Plataforma.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Conteúdo. Propriedade Intelectual  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Todos os textos, gráficos, interfaces de usuários, interfaces visuais, fotografias, marcas registradas, logotipos, sons, músicas e códigos de computador (coletivamente, “Conteúdo”), incluindo, mas não se limitando ao design, estrutural, visual e arranjo do Conteúdo contido na Plataforma Scallar são de propriedade ou licenciado pela ou para Licenciante e está protegido pelas leis de propriedade industrial e direitos autorais e demais leis vigentes aplicáveis.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Exceto conforme expressamente disposto nestes Termos de Uso, nenhuma parte da Plataforma Scallar e nenhum Conteúdo poderá ser copiado, reproduzido, republicado, carregado, publicado, exibido publicamente, codificado, decodificado, traduzido, transmitido ou distribuído de qualquer forma, incluindo engenharia reversa, para qualquer outro computador, servidor, site ou outro meio para publicação, distribuição, seja gratuita ou onerosa, ou para comercialização, no todo ou em parte, para qualquer terceiro, sem o prévio e expresso consentimento da Licenciante.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você poderá utilizar informações sobre produtos e serviços da Licenciante oferecidos na Plataforma Scallar para os propósitos específicos informados, desde que você (1) não remova qualquer linguagem de aviso de propriedade em todas as cópias dos documentos; (2) usar essas informações apenas para seu propósito pessoa e não comercial de tais informações e não copiar, publicar ou distribuir tais informações em qualquer computador em rede ou transmiti-lo em qualquer mídia; (3) não fazer modificações em tais informações e (4) não fazer quaisquer representações adicionais ou garantias relativas a tais documentos.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Comunicações  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   O uso da Plataforma Scallar, envio de e-mails através da Plataforma Scallar, são comunicações eletrônicas feitas por você para a Licenciante. Você concorda em receber comunicações da Licenciante em formato eletrônico. A Licenciante se comunica com seus usuários, clientes e parceiros através de e-mails, postando avisos na Plataforma Scallar. Você concorda que todos os contratos, avisos, divulgações e outras comunicações fornecidas pela Licenciante a você serão feitas eletronicamente e atendem qualquer requisito legal de que tais comunicações sejam feitas por escrito e tenham validade perante terceiros, vinculando as informações ao remetente e destinatário.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Privacidade  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Recomendamos que você leia nossa Política de Privacidade (https://app.scallar.com.br/) para tomar sua decisão ao usar a Plataforma Scallar. Além disso, ao usar a Plataforma Scallar, você reconhece e concorda que as transmissões pela internet nunca são completamente privadas, seguras ou isenta de erros. Você entende que qualquer mensagem ou informação enviada à Plataforma Scallar pode ser lida ou interceptada por terceiros, mesmo que haja um aviso especial de que uma determinada transmissão é criptografada.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Licenciante envia seus melhores esforços e os recursos disponíveis para garantir a segurança de suas informações e de seus usuários, clientes e parceiros, contudo, não se responsabiliza por ataque, invasão ou hackeamento das informações.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Recomendamos que o usuário leia atentamente o item Segurança de Dados. Caso o usuário não concorde com os termos sobre Segurança de Dados, deverá não aceitar ou interromper o uso da Plataforma.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Usos e restrições do Website  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você não pode usar qualquer procedimento, ferramenta, recurso, ou outro dispositivo automático, programa, algoritmo ou metodologia, ou qualquer processo manual similar ou equivalente, para acessar, adquirir, copiar ou monitorar qualquer parte da Plataforma Scallar ou de qualquer Conteúdo, ou de qualquer forma reproduzir ou contornar a estrutura de navegação ou apresentação do Site ou de qualquer conteúdo, para obter ou tentar obter materiais, documentos ou informações por qualquer meio não propositadamente disponibilizado pela Licenciante através da Plataforma Scallar. A Licenciante reserva-se o direito de proibir qualquer atividade nesse sentido e a tomar as providências cabíveis para impedir ou suspender qualquer atividade contrária às disposições destes Termos de Uso.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você não pode tentar obter acesso não autorizado a qualquer parte ou funcionalidade da Plataforma Scallar, ou quaisquer outros sistemas ou redes conectadas à Plataforma Scallar ou a qualquer servidor da Licenciante, ou a qualquer dos serviços oferecidos no ou através da Plataforma Scallar, por hacking ou qualquer outro meio ilegítimo.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você não pode testar ou verificar a vulnerabilidade da Plataforma Scallar ou de qualquer rede conectada ao Site, nem infringir as medidas de segurança ou autenticação do Site ou em qualquer rede conectada à Plataforma Scallar. Você não pode investigar, rastrear ou procurar rastrear qualquer informação de qualquer outro usuário ou visitante da Plataforma Scallar, ou de qualquer outro cliente da Licenciante, incluindo qualquer conta da Licenciante não pertencente a você, à sua fonte ou explorar a Plataforma Scallar ou qualquer serviço ou informação disponibilizada ou oferecida por ou através da Plataforma Scallar, de qualquer forma, quando a finalidade seja revelar qualquer informação, incluindo, mas não se limitando a identificação pessoal ou informação, além da sua própria informação, conforme estabelecido pela Plataforma Scallar.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você concorda que não realizará nenhuma ação que imponha uma carga excessiva ou desproporcionalmente grande na infraestrutura da Plataforma Scallar ou dos sistemas ou redes da Licenciante, ou de quaisquer sistemas ou redes à Plataforma Scallar ao da Licenciante.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você concorda em não usar qualquer dispositivo, software ou rotina para interferir ou tentar interferir no bom funcionamento da Plataforma Scallar ou qualquer transação sendo conduzida na Plataforma Scallar ou com o uso de qualquer outra pessoa da Plataforma Scallar.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você não pode forjar cabeçalhos ou manipular identificadores de modo a disfarçar a origem de qualquer mensagem ou transmissão que você enviar para a Licenciante em ou através da Plataforma Scallar ou qualquer serviço oferecido no ou através da Plataforma Scallar. Você não pode fingir que é, ou que representa, outra pessoa, ou fazer se passar por qualquer outro indivíduo ou entidade.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você não pode usar a Plataforma Scallar ou qualquer Conteúdo para qualquer propósito que seja ilegal ou proibido por estes Termos de Uso ou impostos pela legislação vigente ou para solicitar o desempenho de qualquer atividade ilegal ou outra atividade que infrinja os direitos da Licenciante ou de terceiros.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você concorda em não usar a Plataforma Scallar para (a) prática de quaisquer atos ilícitos e/ou violação da legislação vigente, inclusive das disposições da Lei 9.613/98 e da Lei 12.846/13; (b) carregamento, envio e/ou transmissão de qualquer conteúdo que promova ou incite o preconceito (inclusive de origem, raça, sexo, cor, orientação sexual e idade) ou qualquer forma de discriminação, bem como o ódio ou atividades ilegais; (c) atos contrários à moral e aos bons costumes; (d) prática de atos de ameaça, coação, constrangimento físico ou moral ao Cedente ou quaisquer terceiros e; (e) implique em  violação de direitos de terceiros;  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você concorda ainda que:  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (a) Você tem pelo menos 18 (dezoito) anos de idade ou que é considerado civilmente capaz, e possui legitimidade e poderes suficientes para firmar os Termos de Uso da Plataforma Scallar, bem como para realizar negócio jurídico, por conta própria ou em nome da pessoa ou entidade por ele representada;  ' +
  '     ' +
  '   (b) Você leu e concorda com os Termos de Uso e Política de Privacidade e cumprirá todas as obrigações e restrições;  ' +
  '     ' +
  '   (c) A sua concordância, reconhecimento e aceitação de quaisquer produtos, serviços, termos, condições, preços e/ou restrições podem ser evidenciadas clicando em “Concordo” ou “Aceitar” ou “Enviar” ou “Comprar” ou “Concluído” e você concorda que tal ação será considerada, tratada e terá validade jurídica da mesma forma como se o documento tivesse sido assinado fisicamente.  ' +
  '\n' +
  '     ' +
  '   (d) Você usará a Plataforma Scallar para seu próprio uso, ou para uso daquele a quem você legal e legitimamente representar, e não fornecerá informações sobre qualquer outra pessoa ou que seja impreciso e/ou incompleto ou a conduzir atividades fraudulentas;  ' +
  '     ' +
  '   (e) Você não irá alterar de qualquer forma a Plataforma Scallar.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Links para Sites de terceiros  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Plataforma Scallar da Licenciante poderá conter links para outros Sites de terceiros independentes (“Sites Vinculados”). Esses Sites Vinculados são fornecidos apenas como uma conveniência para você. Esses Sites Vinculados não estão sob o controle da Licenciante, e a Licenciante não é responsável e não endossa o conteúdo de tais Sites Vinculados, incluindo quaisquer informações ou materiais contidos nesses Sites Vinculados. Se você usar os Sites Vinculados, você deixará a Plataforma Scallar da Licenciante e estará sujeito aos termos de uso e à política de privacidade aplicáveis por esses Sites. A decisão de utilizar tais Sites Vinculados será única e exclusivamente do usuário.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Transações na Plataforma Scallar  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Quaisquer transações de produtos ou serviços disponíveis através da Plataforma Scallar (“Transações”) estão sujeitas aos Termos de Uso da Licenciante, à Política de Privacidade e a todas as isenções de responsabilidade e termos e condições aplicáveis que aparecem em qualquer outro local da Plataforma Scallar ou do site da Licenciante. As Transações podem incluir limitações de datas, horários, serviços ou disponibilidade indicadas nos detalhes, termos específicos ou comunicação eletrônica relacionada que o usuário receber da Licenciante.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Plataforma Scallar oferece pacote de serviço gratuito (Plano Free) e pacotes de serviços pagos (verifique a disponibilidade e condições dos planos). A Licenciante se reserva ao direito de alterar as condições de oferecimento da Plataforma, inclusive a cobrança dos serviços, a qual será previamente informada ao usuário, que poderá decidir se continuará a utilizar a Plataforma ou se encerrará sua conta.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Desde já, você concorda que todas as Transações não são reembolsáveis, não podem ser transferidas e podem ser limitadas pelos detalhes da transação. Sujeito à disponibilidade e à critério exclusivo da Licenciante, você poderá pagar pelo produto ou transação de serviço.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Após efetuar a compra, a Licenciante verificará as informações fornecidas para validação, verificando forma de pagamento e endereço de entrega. A Licenciante se reserva o direito de rejeitar ou cancelar qualquer pedido de produto ou serviço caso seja constatada divergência de informações, falha no pagamento ou indício de fraude. Caso o pedido seja rejeitado, a Licenciante notificará o usuário indicando o motivo do cancelamento do pedido.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Aceitação e Confirmação  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você concorda expressamente que o uso desta Plataforma Scallar é por seu único risco. A Licenciante, qualquer dos seus colaboradores, agentes, comerciantes, fornecedores de conteúdo de terceiros ou licenciantes, ou qualquer de seus administradores, prepostos, empregados ou agentes, não garantem que o uso da Plataforma Scallar será ininterrupto ou livre de erros; nem fazer qualquer garantia sobre (i) os resultados que podem ser obtidos do uso desta Plataforma Scallar, ou (ii) a exatidão, integridade, confiabilidade ou conteúdo de qualquer informação, produtos ou operações fornecidas através desta Plataforma Scallar. A Plataforma Scallar e todo o conteúdo e outras informações contidas na Plataforma Scallar, e os produtos e serviços acessíveis ou disponíveis através da Plataforma Scallar são fornecidos ou disponíveis “tal como estão” e “conforme” a base. Todas as informações fornecidas na Plataforma Scallar estão sujeitas a alterações sem aviso prévio.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Licenciante jamais disponibilizará por iniciativa própria arquivo malicioso ou prejudicial ao usuário. Sempre que a Licenciante identificar uma divergência serão tomadas todas as providências para que a mesma seja corrigida. Contudo, a Licenciante não garante que invasões com esse propósito não possam ocorrer.   ' +
  '\n' +
  '    A Licenciante não se responsabiliza pelos atos, omissões e conduta de terceiros relacionados ao uso da Plataforma Scallar e/ou qualquer serviço da Licenciante. O usuário assume total responsabilidade pela utilização da Plataforma Scallar e Sites Vinculados.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Licenciante reserva-se o direito de fazer qualquer um dos seguintes procedimentos, a qualquer momento e sem aviso prévio: (1) modificar, suspender ou encerrar a operação ou acesso à Plataforma Scallar, ou a qualquer parte da Plataforma Scallar, por qualquer motivo; (2) modificar ou alterar a Plataforma Scallar, ou qualquer parte da Plataforma Scallar, e quaisquer políticas ou termos aplicáveis; e (3) interromper a operação da Plataforma Scallar, ou qualquer parte da Plataforma Scallar, conforme necessário para realizar manutenção preventiva, periódica ou esporádica, correção de erros ou outras alterações.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Conta e Senha  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Como parte do seu uso da Plataforma Scallar, você pode receber uma senha e designação de conta ao completar o processo de registro da Plataforma Scallar. Você é responsável por manter a confidencialidade da senha e conta e é totalmente responsável por todas as atividades que ocorrem sob sua senha ou conta.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você concorda em:  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (a) Notificar imediatamente a Licenciante sobre qualquer uso não autorizado da sua senha ou conta ou qualquer outra quebra de segurança de que tome conhecimento;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (b) Sair de sua conta de Usuário ao final de cada sessão e assegurar que esta não seja acessada por terceiros não autorizados. A Licenciante não será responsável por qualquer perda ou dano decorrente do descumprimento do disposto nestes Termos de Uso por parte do Usuário.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Licenciante não pode e não será responsável por qualquer perda ou dano decorrente da falha na correta proteção de sua conta ou senha. Você é o único responsável pela proteção e backup adequados do Conteúdo. A Licenciante não será responsável por utilização ilegal, fraudulenta ou que causem danos e prejuízos ao empregador ou entidade representada pelo usuário, realizadas por usuários por eles nomeados.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Limitação de Responsabilidade  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Na medida do permitido pela legislação aplicável e na medida em que a Licenciante tenha, de outra forma, sido julgada responsável por quaisquer danos, a Licenciante responderá tão somente por danos concretos. Na medida do permitido em lei, a Licenciante, seus fornecedores ou quaisquer terceiros mencionados na Plataforma Scallar não deverão, em hipótese alguma, responder por danos eventuais, indiretos e emergentes, lucros cessantes ou danos decorrentes de perda de dados ou interrupção dos negócios como consequência do uso ou de incapacidade quanto ao uso da Plataforma Scallar, dos produtos e serviços oferecidos pela Licenciante, informações ou do conteúdo, seja com fundamento em garantia, contrato, ato ilícito, delito ou em qualquer outra tese jurídica, e independentemente de a possibilidade de tais danos ter sido, ou não, levada ao conhecimento da Licenciante. Na medida do permitido em lei, os recursos legais consignados para você nestes Termos de Uso são exclusivos e limitados àqueles expressamente previstos nestes Termos.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Isenção de Responsabilidade Quanto a Garantias  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Licenciante não formula nenhuma declaração acerca dos resultados a serem obtidos a partir do uso da Plataforma Scallar, dos serviços, das informações ou do conteúdo. O seu respectivo uso corre por sua própria conta e risco. Toda a Plataforma Scallar, as informações, os serviços e o Conteúdo da Plataforma são fornecidos “no estado em que se encontra” ou “como estão” sem garantia de qualquer natureza. A Licenciante não garante que (1) o serviço atenderá às suas exigências, (2) o serviço será ininterrupto, oportuno, seguro ou sem erros, (3) a qualidade de qualquer produto, serviço, informação ou outro material obtido por você pelo serviço atenderá às suas expectativas.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Em nenhuma hipótese, a Licenciante, seus fornecedores ou terceiros mencionados na Plataforma Scallar serão responsáveis:  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (a) Por qualquer ato ou omissão realizado e/ou dano causado pelo usuário decorrente do acesso à Plataforma Scallar;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (b) Pelo uso indevido da Plataforma Scallar por qualquer usuário ou terceiros e/ou pelos conteúdos carregados, enviados e/ou transmitido à Plataforma pelo usuário;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (c) Por falhas, impossibilidades técnicas ou indisponibilidades do sistema;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (d) Pela instalação no equipamento do Usuário ou de terceiros, de vírus, trojans, malware, worm, bot, backdoor, spyware, rootkit, ou de quaisquer outros arquivos que venham a ser criados, em decorrência da navegação na Internet pelo usuário.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Segurança da Informação  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Segurança de dados. Implementamos e mantemos procedimentos físicos, eletrônicos e gerenciais destinados a proteger contra a perda, uso indevido, acesso não autorizado, alteração ou divulgação de dados do assinante. Essas medidas incluem criptografia de dados de assinante durante a transmissão do serviço e criptografia de backups de dados do assinante e credenciais de autenticação em repouso. Nós notificaremos você de qualquer acesso ou uso não autorizado de dados do assinante que chame nossa atenção. Se alguma divulgação não autorizada dos dados do assinante resultantes do seu uso do serviço chame nossa atenção, trabalharemos com você para investigar a causa dessa divulgação não autorizada, e trabalharemos em conjunto e de boa-fé para tomar as medidas razoavelmente necessárias para evitar qualquer futuro reincidência e em conformidade com as leis aplicáveis à notificação de violação de dados.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Transmissão de dados. Você reconhece que o uso dos serviços envolve transmissão de dados do assinante e outras comunicações através da internet e outras redes sociais, e que tais transmissões podem potencialmente ser acessadas por partes não autorizadas. Você deve proteger login de usuário autorizado e senhas do acesso ou uso por pessoas não autorizadas e será o único responsável por qualquer falha na proteção e uso de login e senha. Você deverá nos notificar imediatamente em caso de suspeita de quebra de segurança através do e-mail contato@scallar.com.br   ' +
  '\n' +
  '    Os dados do assinante são sua propriedade. Você nos concede uma licença não-exclusiva, mundial, isenta de royalties para usar, copiar, transmitir, sublicenciar, indexar, armazenar, agregar e exibir os dados do assinante conforme necessário para fornecer ou executar os serviços ora contratados, serviços técnicos de suporte e de gerenciamento de contas, e para publicar, exibir e distribuir informações agregadas derivadas dos dados do assinante e do seu uso do serviço para melhorar nossos produtos, serviços e desenvolvimento, exibição e distribuição de benchmarks e relatórios similares, desde que esses dados não sejam publicamente identificados ou identificáveis como originários ou associados a você ou a qualquer pessoa individualmente.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Responsabilidades do usuário  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   O Usuário é exclusivamente responsável:  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (a) Por todos e quaisquer atos ou omissões por ele realizados a partir de seu acesso à Plataforma Scallar;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (b) Por todo e qualquer conteúdo por ele carregado, enviado e/ou transmitido à Plataforma Scallar;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (c) Pela reparação de todos e quaisquer danos, diretos ou indiretos (inclusive decorrentes de violação de quaisquer direitos de outros usuários, de terceiros, inclusive direito de propriedade intelectual, de sigilo e de personalidade), que sejam causados à Licenciante, a qualquer outro Usuário, ou, ainda, a qualquer terceiro, inclusive em virtude do descumprimento do disposto nestes Termos de Uso e Política de Privacidade ou de qualquer ato praticado a partir de seu acesso à Plataforma Scallar.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Violação aos Termos de Uso  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você reconhece e concorda que a Licenciante pode recolher, armazenar e avaliar informações e transações feitas por você com a Licenciante através da Plataforma Scallar ou qualquer serviço oferecido na Plataforma Scallar e também poderá revelar tais dados caso venha ser exigido por lei ou por decisão administrativa ou judicial.   ' +
  '\n' +
  '    Você concorda que a Licenciante poderá, a seu exclusivo critério e sem aviso prévio, encerrar seu acesso à Plataforma Scallar e/ou bloquear seu acesso futuro à Plataforma Scallar se for constatada a violação destes Termos de Uso ou outros acordos ou diretrizes que podem ser associado ao seu uso da Plataforma Scallar. Você também concorda que qualquer violação destes Termos de Uso constituirá uma prática comercial ilegal e causará danos irreparáveis à Licenciante, ficando sujeito a reparar os danos e prejuízos sofridos pela Licenciante.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Rescisão  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Você concorda que a Licenciante pode, em qualquer circunstância e sem aviso prévio, encerrar imediatamente sua conta e/ou acesso ao Serviço e/ou qualquer Conteúdo nos casos de:  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (a) Violações ou violações dos Termos de Serviço ou de outros acordos ou diretrizes incorporados;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (b) Solicitações de autoridades governamentais ou por decisão judicial;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (c) Problemas técnicos ou de segurança inesperados;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (d) Prolongados períodos de inatividade;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (e) Envolvimento do usuário atividades fraudulentas ou ilegais através dos serviços oferecidos pela Licenciante;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (f) Não pagamento de quaisquer taxas devidas pelo usuário em conexão com os Serviços.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   O encerramento da conta no Site inclui:  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (a) Remoção do acesso a todas as ofertas na Plataforma Scallar;  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (b) Eliminação de login e senhas e todas as informações relacionadas, pastas e conteúdos associados ou dentro da sua conta (ou qualquer parte dela);  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   (c) Proibição de utilização posterior da Plataforma Scallar até que o usuário manifeste expressamente sua vontade de revalidar o contrato e sujeitar-se aos Termos de Uso. Em caso de reincidência nas infrações descritas nos itens (a), (e) e (f) acima, o usuário ficará definitivamente proibido de utilizar a Plataforma posteriormente.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Em caso de rescisão a Licenciante não será responsável perante o usuário ou terceiros pelo encerramento da conta, qualquer endereço de e-mail associado ou acesso à Plataforma Scallar ou Conteúdo.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Legislação e Foro Aplicáveis  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Todas as questões relacionadas ao acesso ou uso da Plataforma Scallar, aos Termos de Uso, Política de Privacidade e demais cláusulas e condições previstas serão regidas e interpretadas pelas leis da República Federativa do Brasil, inclusive em eventuais ações decorrentes de violação dos seus termos e condições.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Fica eleito o Foro da Comarca de Pelotas no Estado do Rio Grande do Sul para dirimir quaisquer dúvidas, questões ou litígios decorrentes do presente Contrato, renunciando as partes a qualquer outro, por mais privilegiado que seja.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Abrangência  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   A Licenciante está sediada no Estado do Rio Grande do Sul – Brasil, administra e opera a Plataforma Scallar a partir de sua localização. Embora a Plataforma Scallar seja acessível em todo o mundo, nem todos os recursos, produtos ou serviços oferecidos através ou na Plataforma Scallar estão disponíveis para todas as pessoas ou em todas as localidades geográficas ou disponíveis para uso fora do Brasil. A Licenciante reserva-se o direito de limitar, a seu exclusivo critério, a provisão e a quantidade de qualquer característica, produto ou serviço a qualquer pessoa ou área geográfica. Qualquer oferta para qualquer recurso, produto ou serviço feito na Plataforma Scallar é nula no país ou localidade onde for proibido. Se você optar por acessar a Plataforma Scallar de fora do Brasil, o faz por sua própria iniciativa e você é o único responsável pelo cumprimento das leis locais aplicáveis.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Disposições Gerais  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Validade do Instrumento. Os signatários declaram que possuem plena capacidade civil, sem qualquer impedimento legal.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Tolerância. Qualquer tolerância pelas Partes em relação aos Termos de Uso, Política de Privacidade e demais documentos relacionados aos produtos e serviços oferecidos pela Licenciante, ou mesmo o retardamento da exigibilidade de direitos, não importará em precedente, novação, moratória ou alteração, permanecendo todos os termos plenamente exigíveis e exequíveis. Estes Termos de Uso não devem ser interpretados ou interpretados para conferir quaisquer direitos ou reparação a terceiros.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Nulidade. Qualquer cláusula ou condição destes Termos de Uso, Política de Privacidade e demais documentos relacionados que, por qualquer razão, venha a ser reputada nula ou ineficaz por qualquer juízo ou tribunal, não afetará a validade das demais disposições, as quais permanecerão plenamente válidas e vinculantes, gerando efeitos em sua máxima extensão.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Totalidade de entendimentos. Estes Termos de Uso constituem o acordo completo entre você e a Licenciante com relação ao seu uso da Plataforma Scallar, e todos e quaisquer outros acordos ou entendimentos escritos ou verbais anteriormente existentes entre você e a Licenciante com relação a tal uso são substituídos e cancelados.   ' +
  '\n' +
  '    Caso fortuito e força maior. Não obstante qualquer disposição aqui contida, a Licenciante não estará inadimplente perante você, na medida em que a observância ou cumprimento de quaisquer termos ou disposições destes Termos de Uso, Política de Privacidade ou documentos relacionados for postergado ou evitado por revolução ou outra perturbação civil; guerras; atos de inimigos; greves; terrorismo; reclamações trabalhistas; falhas elétricas ou de disponibilização de telecomunicações; incêndios; inundações; casos fortuitos; ou, sem limitar o acima exposto, quaisquer outros casos que não estejam dentro de seu controle, e que, pelo exercício de zelo razoável, sejam incapazes de evitar, quer da classe das causas aqui acima enumeradas ou não.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Publicidade. A Licenciante poderá usar apenas seu nome e/ou marcas na medida necessária para cumprir as obrigações relacionadas aos serviços ora contratados. Mediante a aceitação das condições destes Termos de Uso, reservamo-nos o direito de usar o seu nome e/ou marca como referência para fins de marketing e promoção em nosso site, na Plataforma Scallar e em outros meios de comunicações relacionados aos serviços prestados para nossos atuais e futuros clientes. Se você não quiser ser listado como referência pelos serviços contratados, você pode enviar um e-mail para contato@scallar.com.br informando que não deseja ser identificado como uma referência.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Resposta e Informação  ' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Qualquer comentário que você fornecer ou fizer publicar na Plataforma Scallar será considerado não confidencial. A Licenciante terá liberdade para usar essas informações sem restrições.  ' +
  '\n' +
  '     ' +
  '\n\n' +
  '     ' +
  '   Preços e disponibilidade de produtos e serviços estão sujeitos a alterações sem aviso prévio.  ' +
  '\n' +
  '     ' +
  '   contato@scallar.com.br  ' +
  '\n' +
  '     ' +
  '    ';
