export const PoliticaDePrivacidade =
  '   ACORDO SOBRE OS TERMOS DE USO E PRIVACIDADE DO SITE E PLATAFORMA SCALLAR  ' +
  '\n\n' +
  '   Política de Privacidade e Segurança de Dados  ' +
  '\n\n' +
  '   Última atualização: 13 de novembro de 2019   ' +
  '\n\n' +
  '     ' +
  '   A presente Política de Privacidade e Segurança de Dados do Sistema Scallar Dashboards integra um conjunto de ações que a Scallar estabeleceu de acordo as melhores práticas e calcados em sua filosofia de transparência e respeito aos clientes/usuários, com o objetivo de respeitar e preservar o direito ao sigilo de identidade, contendo diretrizes e informações sobre como a Scallar recepciona, armazena e utiliza as informações pessoais fornecidas e/ou cadastradas pelos clientes/usuários/internautas para acesso e uso do Sistema Scallar, e qual a sua responsabilidade sobre a guarda, proteção e divulgação de tais informações.  ' +
  '\n' +
  '\n\n' +
  '   DEFINIÇÕES  ' +
  '\n\n' +
  '   Sem prejuízo de outras definições constantes nesta Política, os seguintes termos terão os significados a eles designados abaixo:  ' +
  '\n ' +
  '   “Sistema”: Toda e qualquer solução online, proprietária ou de terceiros, disponibilizada via aplicativo ou Sistema da Scallar.  ' +
  '\n' +
  '\n' +
  '   “Internauta”: todos aqueles que, de alguma forma, acessam o Sistema Scallar, sendo cliente ou não.  ' +
  '\n' +
  '\n' +
  '   “Cliente”: todo aquele que utiliza os produtos e/ou serviços oferecidos pelo Sistema Scallar, pagos ou não.  ' +
  '\n' +
  '\n' +
  '   “Usuário”: todo aquele que se cadastrar neste Sistema e receber uma identificação individual e exclusiva.  ' +
  '\n' +
  '\n' +
  '   “Dado pessoal”: qualquer informação relativa a uma pessoa identificada ou identificável, incluindo seu nome completo ou nome comercial, endereço físico e eletrônico, número de telefone, RG, CPF ou CNPJ, número de cartão de crédito, situação financeira, patrimonial, contrato social, balanço patrimonial, preferências e padrões de acesso, incluindo todo endereço ou número de identificação de um terminal utilizado para conexão a uma rede de computadores. Tais informações, quando coletadas, serão armazenadas, utilizando-se de rígidos padrões de sigilo e integridade, bem como controles de acesso físico e lógico, observando-se sempre os mais elevados princípios éticos e legais. Apesar de adotar altos níveis de segurança para a proteção dos dados e informações coletados, é importante que os clientes/usuários/internautas tenham ciência de que, pela própria natureza e características técnicas da internet, há sempre o risco de que terceiros não autorizados, de alguma forma, consigam violar esses mecanismos de proteção e ter acesso a tais informações, motivo pelo qual a Scallar não pode garantir a total privacidade e segurança na utilização do aplicativo ou no armazenamento dos dados.  ' +
  '\n' +
  '\n' +
  '   “Criptografia”: nome dado ao processo de codificação de informações. As informações são codificadas (embaralhadas) na origem e decodificadas no destino, dificultando, dessa forma, que sejam decifradas durante o tráfego na internet.  ' +
  '\n' +
  '\n' +
  '   “Cookies”: arquivos-texto armazenados pelo navegador (exemplo, Internet Explorer, Firefox, Chrome ou Safari) em seu computador ou dispositivo móvel. Os cookies têm como finalidade identificar o computador ou dispositivo móvel, personalizar os acessos e obter dados de acesso, como páginas navegadas ou links clicados. Um cookie é atribuído individualmente a cada computador, não podendo ser usado para executar programas, tampouco infectar computadores com vírus, trojans, etc., e podem ser lidos apenas pelo servidor que o enviou.  ' +
  '\n' +
  '\n' +
  '     ' +
  '   COMPROMISSO  ' +
  '\n\n' +
  '   A SCALLAR está comprometida em:  ' +
  '\n' +
  '   Coletar em seu Sistema apenas as informações sobre identificação individual necessárias à viabilização de seus negócios e ao fornecimento de produtos e/ou serviços solicitados por seus clientes/usuários.  ' +
  '\n' +
  '\n' +
  '   Solicitar autorização para envio de material informativo ou promocional ao correio eletrônico dos internautas cadastrados em seu Sistema.  ' +
  '\n' +
  '\n' +
  '   Possibilitar o cancelamento de autorização fornecida anteriormente para o envio de material informativo ou promocional.  ' +
  '\n' +
  '\n' +
  '   Utilizar cookies apenas para controlar a audiência e a navegação em seu Sistema. Os “cookies” não serão usados para controlar ou utilizar os dados pessoais do usuário, exceto quando esse desrespeitar alguma regra de segurança ou exercer alguma atividade prejudicial ao bom funcionamento do Scallar, como, por exemplo, tentativas de “hackear”, de invadir o Sistema.  ' +
  '\n' +
  '\n' +
  '   Gravar as ligações telefônicas originadas pelos contatos dos internautas/usuários com os anunciantes Pessoa Jurídica, única e exclusivamente para fins de controlar a quantidade de contatos realizados com cada anunciante Pessoa Jurídica, objetivando a operacionalização interna da cobrança destes anunciantes.  ' +
  '\n' +
  '\n' +
  '   Cumprir rigorosamente todas as determinações desta Política de Privacidade e Segurança de Dados.  ' +
  '\n' +
  '\n' +
  '     ' +
  '   PRIVACIDADE DE DADOS  ' +
  '\n\n' +
  '   CAPTURA DE DADOS  ' +
  '\n\n' +
  '   Dependendo da ação realizada, ao se cadastrar, navegar, adquirir ou solicitar produtos e/ou serviços do Sistema Scallar, serão requisitadas informações sobre:  ' +
  '\n\n' +
  '   Identificação individual do cliente;  ' +
  '\n' +
  '   Contas de acesso com permissão de leitura para captura de dados de campanhas no Google Ads;  ' +
  '\n' +
  '   Contas de acesso com permissão de leitura para captura de dados de campanhas no Google Analytics;  ' +
  '\n' +
  '   Contas de acesso com permissão de escrita no Google Drive para criação de pastas e planilha de integração de dados com Google Sheets, esse acesso será utilizado apenas para este fim e nada mais;  ' +
  '\n' +
  '   Contas de acesso com permissão de leitura para captura de dados de campanhas do Facebook Ads;  ' +
  '\n' +
  '   Contas de acesso com permissão de leitura para captura de dados de campanhas do Instagram Insights;  ' +
  '\n' +
  '   Contas de acesso com permissão de leitura para captura de dados de campanhas do Facebook Insigths;  ' +
  '\n' +
  '   Aquisição e forma de pagamento do produto ou serviço solicitado no Sistema;  ' +
  '\n' +
  '   Preferências do cliente;  ' +
  '\n' +
  '   Opiniões do internauta;  ' +
  '\n' +
  '   Acesso do internauta (exemplo: data e horário de realização do acesso);  ' +
  '\n' +
  '   Perfil de acesso do internauta (exemplo: links clicados, páginas visitadas).  ' +
  '\n' +
  '\n' +
  '     ' +
  '   UTILIZAÇÃO DE DADOS  ' +
  '\n ' +
  '   As informações capturadas por meio do Sistema Scallar são utilizadas com a finalidade de:  ' +
  '\n' +
  '   Apuração estatística geral;  ' +
  '\n' +
  '   Criação de dashboards integrados com os resultados das campanhas de mídia digital  ' +
  '\n' +
  '   Maior e melhor interatividade do Sistema ao usuário;  ' +
  '\n' +
  '   Aperfeiçoar a usabilidade e a experiência interativa na utilização do Sistema;  ' +
  '\n' +
  '   Possibilidade de ofertas e/ou informações mais assertivas, relevantes às necessidades e/ou interesses do cliente/usuário/internauta;  ' +
  '\n' +
  '   Possibilidade de maior eficiência em relação à frequência e continuidade de comunicação com o usuário;  ' +
  '\n' +
  '   Maior e melhor experiência do consumidor no nosso Sistema e/ou em sua navegação pela internet ;  ' +
  '\n' +
  '   Responder as dúvidas e solicitações dos usuários;  ' +
  '\n' +
  '   Realizar pesquisas de comunicação e marketing de relacionamento;  ' +
  '\n' +
  '   Divulgar alterações, inovações ou promoções sobre os produtos e serviços da Scallar.  ' +
  '\n' +
  '\n' +
  '   Permitir aos internautas navegar ou realizar as operações disponibilizadas no Sistema;  ' +
  '\n' +
  '   Viabilizar o fornecimento de produtos ou serviços solicitados no Sistema;  ' +
  '\n' +
  '   Identificar o perfil, desejos ou necessidades dos internautas, a fim de aprimorar os produtos e/ou serviços oferecidos pela empresa;  ' +
  '\n' +
  '   Enviar informativos sobre produtos ou serviços aos seus clientes;  ' +
  '\n' +
  '\n' +
  '   DIVULGAÇÃO DE DADOS  ' +
  '\n\n' +
  '   A Scallar não fornece dados a terceiros sobre a identificação individual de clientes/usuários/internautas, sem seu prévio consentimento, ressalvadas as hipóteses abaixo:  ' +
  '\n' +
  '      ' +
  '   A Scallar informa a todos os usuários/clientes/internautas que coleta, usa, armazena, trata e compartilha seus dados pessoais e de contato, inclusive registros de conexão e de acesso à aplicações de internet, nos produtos e/ou serviços disponibilizados neste Sistema;  ' +
  '\n' +
  '   Em casos em que haja determinação legal ou judicial para fornecimento de dados;  ' +
  '\n' +
  '   A Scallar se compromete a usar as informações coletadas apenas para as finalidades aqui definidas, reservando-se o direito de guardá-las para fins de cumprimento de Lei e/ou para resguardar em caso de qualquer pretensão civil, bem como destruí-las após sua utilização, ou simplesmente não utilizá-las.  ' +
  '\n' +
  '\n' +
  '   A Scallar sempre respeitando a privacidade de seus usuários, uma vez provida das informações pessoais a respeito do cliente/usuário/internauta, poderá utilizar os dados do usuário para o fim de enviar publicidade, direcionada por e-mail ou por quaisquer outros meios de comunicação, contendo informações sobre a Scallar, suas empresas parceiras, seus produtos e serviços. Entretanto, fica reservado ao cliente/usuário/internauta o direito de, a qualquer momento, inclusive no ato da disponibilização das informações pessoais, informar à Scallar, por meio dos canais de comunicação disponíveis para o cadastramento de tais informações, do não interesse em receber tais anúncios, inclusive por e-mail (opt-out), hipótese em que a Scallar encerrará tais envios no menor tempo possível. Caso não deseje mais receber nossos e-mails, basta selecionar no cabeçalho ou rodapé de cada e-mail a opção “Descadastre-se”. Você será redirecionado à página de confirmação do cancelamento.  ' +
  '\n' +
  '\n' +
  '   Para fins de avaliação de crédito, verificação e gestão de risco, poderão suas informações ser trocadas com fontes de referência respeitáveis, órgãos reguladores e serviços de compensação.  ' +
  '\n' +
  '\n' +
  '     ' +
  '   SEGURANÇA DAS INFORMAÇÕES CONFIDENCIAIS  ' +
  '\n\n' +
  '   O acesso às informações pessoais coletadas e armazenadas pela Scallar é restrito aos profissionais autorizados ao uso direto dessas informações, e necessário à prestação de seus serviços, sendo limitado o uso para outras tarefas. É exigido, também, de toda organização ou indivíduo contratado para a prestação de serviços de apoio, que sejam cumpridos nossos padrões de privacidade e as Políticas de Segurança da Informação então vigentes.  ' +
  '\n' +
  '     ' +
  '   A Scallar trabalha exaustivamente para assegurar que as informações divulgadas para os clientes sejam verdadeiras e íntegras, contando com controles apurados de monitoramento das informações fornecidas. Sua participação no processo é revisar as informações, valores e informativos, comunicando-nos qualquer discrepância nas informações fornecidas.  ' +
  '\n' +
  '     ' +
  '   A Scallar não responderá por prejuízos que possam advir do vazamento das informações pessoais por violação ou quebra das barreiras de segurança de internet por terceiros como “hackers” ou “crackers”.  ' +
  '\n' +
  '     ' +
  '   Além dos casos acima citados, havendo a necessidade ou interesse em repassar a terceiros dados de identificação individual dos clientes/usuários/internautas, a Scallar lhes solicitará autorização prévia. Informações que não sejam de identificação individual (como demográficas, por exemplo), poderão ser repassadas a anunciantes, fornecedores, patrocinadores e parceiros, com o objetivo de melhorar a qualidade dos produtos e serviços oferecidos pela Scallar.  ' +
  '\n' +
  '     ' +
  '   Aos terceiros que, porventura receberem da Scallar informações de qualquer natureza, sobre os internautas que acessam o seu Sistema, cabe igualmente, a responsabilidade de zelar pelo sigilo e segurança de referidas informações.  ' +
  '\n' +
  '     ' +
  '   A Scallar se empenha expressivamente para prover segurança e sigilo das informações que capta. Contudo, para que as medidas adotadas tenham eficácia, faz-se necessário que cada cliente/usuário/internauta também tenha atitude responsável, sendo cuidadoso com os dados de sua identificação individual sempre que acessar a internet, informando-os somente em operações em que exista a proteção de dados, nunca divulgando sua identificação de usuário e sempre desconectando a conta do Sistema Scallar tão logo deixe de acessá-la, principalmente se dividir o Dispositivo móvel/computador com outra(s) pessoa(s).  ' +
  '\n' +
  '     ' +
  '   A Scallar assegura que as informações (textos, imagens, sons e/ou aplicativos) contidas nos seus sites estão de acordo com a legislação e normativos que regulam os direitos autorais, marcas e patentes, não sendo permitidas modificações, cópias, reproduções ou quaisquer outras formas de utilização para fins comerciais sem o consentimento prévio e expresso da Scallar.  ' +
  '\n' +
  '     ' +
  '   A Scallar não se responsabiliza por eventuais danos e/ou problemas decorrentes da demora, interrupção ou bloqueio nas transmissões de dados ocorridos na Internet.  ' +
  '\n' +
  '\n' +
  '   EXTENSÃO DOS EFEITOS  ' +
  '\n\n' +
  '   Os termos da Política de Privacidade e Segurança de Dados aqui expostos aplicar-se-ão exclusivamente às informações pessoais, conforme acima definido, que venham a ser disponibilizadas à Scallar, pelo cliente/usuário/internauta para a utilização de seus produtos e serviços. Por consequência, a Política de Privacidade e Segurança de Dados aqui exposta não será aplicável a qualquer outro serviço que não os disponibilizados pela Scallar, incluídos aqueles sites que estejam de alguma forma vinculados ao site e ao aplicativo da Scallar, através de links ou quaisquer outros recursos tecnológicos, e, ainda, a quaisquer outros sites que, de qualquer forma, venham a ser conhecidos ou utilizados através da Scallar.  ' +
  '\n' +
  '     ' +
  '   Nesse sentido, a Scallar alerta aos usuários que os referidos sites podem conter política de privacidade diversa da adotada pela Scallar ou podem até mesmo não adotar qualquer política nesse sentido, não se responsabilizando, a Scallar, por qualquer violação aos direitos de privacidade dos usuários que venham a ser violados pelos referidos sites.  ' +
  '\n' +
  '\n' +
  '   NOTÍCIAS E PUBLICIDADE  ' +
  '\n\n' +
  '   As newsletters e mensagens publicitárias enviadas por e-mail sempre trarão opção de cancelamento do envio daquele tipo de mensagem por parte da Scallar. Não obstante, a Scallar coloca à disposição de seus clientes/usuários uma ferramenta de gestão das mensagens para que este possa solicitar, a qualquer tempo,o cancelamento do envio de mensagens.  ' +
  '\n' +
  '     ' +
  '   As mensagens enviadas podem conter códigos que permitem personalizar mensagens de acordo com as preferências individuais do cliente/usuário registrado da Scallar e elaborar relatórios sobre a visualização das mensagens, sobre o número de vezes em que o e-mail foi aberto e o número de cliques feitos na mensagem. Estas informações serão utilizadas de forma genérica e agregada para elaborar relatórios sobre o envio de mensagens. Tais relatórios poderão ser repassados aos anunciantes, como indicadores estatísticos da efetividade das campanhas, sem revelar informações pessoais dos clientes/usuários.  ' +
  '\n' +
  '     ' +
  '   Em nenhuma hipótese a Scallar irá divulgar esses dados de forma individualizada, ou seja, não haverá identificação do cliente/usuário ou de seus hábitos de acesso e leitura de e-mails. Ainda que coletivamente, os dados só podem ser utilizados para os fins previstos nesta Política de Privacidade.  ' +
  '\n' +
  '\n' +
  '   SEGURANÇA DE DADOS TRANSACIONAIS  ' +
  '\n\n ' +
  '   PROTEÇÃO DOS DADOS TRANSACIONAIS  ' +
  '\n\n' +
  '   As transações efetuadas para escolha da forma e efetivação do pagamento, independente da forma escolhida, são realizadas em ambiente seguro e certificado por empresa especializada em segurança de dados para a internet. Todas as informações necessárias à realização do pagamento são criptografadas. O ícone “Cadeado Fechado”, localizado na barra de status, é o indicativo de que as informações transacionadas serão criptografadas.  ' +
  '\n' +
  '     ' +
  '   Outra medida de segurança é a não coleta do número e da data de validade de cartão de crédito, e de outras informações específicas para pagamento pela Scallar. Estes dados são informados diretamente para as instituições financeiras e não são armazenados ou utilizados para qualquer outro fim.  ' +
  '\n' +
  '     ' +
  '   Em alguns pontos do Sistema Scallar são coletadas informações de identificação individual e/ou cadastral (como nome completo ou razão social, endereço, telefones, dados de veículo etc.) necessárias à navegação ou utilização dos serviços disponíveis. Como medida de proteção, as informações de identificação individual e/ou cadastral coletadas pelo Sistema Scallar passam por processo de criptografia em todas as páginas de coleta de dados (como o anúncio de um automóvel ou adesão a um plano) e nas áreas seguras do site onde um usuário e senha são solicitados.  ' +
  '\n' +
  '     ' +
  '   As operações realizadas no Sistema Scallar por um usuário, bem como as informações associadas a estas operações, são exclusivas e só podem ser acessadas por este usuário.  ' +
  '\n' +
  '\n' +
  '   IDENTIFICAÇÃO DO USUÁRIO  ' +
  '\n\n' +
  '   Ao se cadastrar no Sistema Scallar para aquisição de produtos ou serviços (pagos ou não) oferecidos no Sistema, cada usuário recebe uma identificação única, identificação esta que passa a ser requerida e autenticada nos demais acessos ao Sistema. Essa identificação, para os fins de direito, serve como assinatura de concordância com qualquer proposta realizada neste Sistema.  ' +
  '\n' +
  '     ' +
  '   A identificação de usuário é exclusiva, intransferível e criptografada para ser transmitida ao servidor da Scallar, sendo a senha armazenada em banco de dados na sua forma criptografada, de maneira irreversível.  ' +
  '\n' +
  '     ' +
  '   A Scallar poderá confirmar os dados pessoais informados pelo cliente/usuário/internauta, consultando órgãos públicos, empresas especializadas, cadastros restritivos ou centrais de risco. As informações que a Scallar obtiver destas entidades serão tratadas de forma confidencial.  ' +
  '\n' +
  '     ' +
  '   Sem prejuízo do disposto anteriormente, o cliente/usuário garante e responde pela veracidade, exatidão, vigência e autenticidade dos dados pessoais, e se compromete a mantê-los devidamente atualizados. Por consequência, a Scallar não possui qualquer responsabilidade no caso de inserção de dados falsos ou inexatidão dos dados pessoais introduzidos por outros clientes/usuários/internautas.  ' +
  '\n' +
  '\n' +
  '   ALTERAÇÕES NA POLÍTICA  ' +
  '\n\n' +
  '   A Scallar poderá alterar esta Política de Privacidade e Segurança de Dados a qualquer momento, em virtude de alterações na legislação ou nos serviços, em decorrência da utilização de novas ferramentas tecnológicas ou, ainda, sempre que, a exclusivo critério da Scallar, tais alterações se façam necessárias, razão pela qual recomendamos a leitura periódica desta Política de Privacidade e Segurança de Dados.  ' +
  '\n' +
  '     ' +
  '   A utilização dos serviços disponibilizados pela Scallar por qualquer cliente/usuário implicará em expressa aceitação quanto aos termos e condições da Política de Privacidade e Segurança de Dados vigente na data de sua utilização.  ' +
  '\n' +
  '     ' +
  '   A Scallar recomenda aos clientes/usuários que não concordem com a Política de Privacidade e Segurança de Dados vigente a não utilização dos serviços deste Sistema.  ' +
  '\n' +
  '     ' +
  '      ' +
  '     ' +
  '   DÚVIDAS E SUGESTÕES  ' +
  ' \n\n' +
  '   Caso haja qualquer dúvida ou sugestão sobre estes Termos da Política de Privacidade e Segurança de Dados, escreva para: contato@scallar.com.br.  ' +
  '\n' +
  '     ' +
  '   Caso se perceba que esta Política de Privacidade e Segurança de Dados não esteja sendo cumprida, a não conformidade detectada deverá ser apontada ao responsável por meio do endereço: contato@scallar.com.br.  ' +
  '\n' +
  '     ' +
  '      ' +
  '     ' +
  '   TERMO DE USO  ' +
  '\n\n' +
  '   O cliente/usuário expressamente aceita os termos e condições elencados no Termo de Uso, os quais ficam aqui incorporados por referência.  ' +
  '\n' +
  '     ' +
  '   https://scallar.com.br/termos-de-uso  ' +
  '\n' +
  '\n\n' +
  '   LEI APLICÁVEL E RESOLUÇÃO DE CONFLITOS  ' +
  '\n\n' +
  '   Toda e qualquer controvérsia oriunda dos termos expostos na presente Política de Privacidade e Segurança de Dados serão solucionados de acordo com a lei brasileira, sendo competente o foro da cidade de Pelotas/RS, Comarca da Capital, com exclusão de qualquer outro por mais privilegiado que seja.  ' +
  '\n' +
  '     ' +
  '   Fica claro, ainda, que utilização de serviços e as ordens comandadas fora do território brasileiro, ou ainda as decorrentes de operações iniciadas no exterior podem estar sujeitas também à legislação e jurisdição das autoridades dos países onde forem comandadas ou iniciadas.  ' +
  '\n' +
  '     ' +
  '    ';
