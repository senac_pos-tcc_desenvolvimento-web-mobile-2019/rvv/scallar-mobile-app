export default [
  {label: 'Janeiro', name: 'janeiro', la: 'Jan', value: 1},
  {label: 'Fevereiro', name: 'fevereiro', la: 'Fev', value: 2},
  {label: 'Março', name: 'marco', la: 'Mar', value: 3},
  {label: 'Abril', name: 'abril', la: 'Abr', value: 4},
  {label: 'Maio', name: 'maio', la: 'Mai', value: 5},
  {label: 'Junho', name: 'junho', la: 'Jun', value: 6},
  {label: 'Julho', name: 'julho', la: 'Jul', value: 7},
  {label: 'Agosto', name: 'agosto', la: 'Ago', value: 8},
  {label: 'Setembro', name: 'setembro', la: 'Set', value: 9},
  {label: 'Outubro', name: 'outubro', la: 'Out', value: 10},
  {label: 'Novembro', name: 'novembro', la: 'Nov', value: 11},
  {label: 'Dezembro', name: 'dezembro', la: 'Dez', value: 12},
];
