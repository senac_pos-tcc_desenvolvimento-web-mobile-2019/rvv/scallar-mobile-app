export default [
  {label: 'Presidente', value: 1},
  {label: 'Gerente', value: 2},
  {label: 'Gerente produto', value: 3},
  {label: 'Gerente financeiro', value: 4},
  {label: 'Gerente planejamento', value: 5},
  {label: 'Analista marketing', value: 6},
  {label: 'Analista financeiro', value: 7},
  {label: 'Assessor', value: 8},
  {label: 'Outro', value: 9},
];
