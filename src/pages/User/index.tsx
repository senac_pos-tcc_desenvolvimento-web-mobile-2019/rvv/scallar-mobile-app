import React from 'react';
import {useAuth} from '../../hooks/Auth';
import {Container, Title} from './styles';
import Button from '../../components/Button';

const User: React.FC = () => {
  const {signout} = useAuth();

  return (
    <Container>
      <Title>User</Title>
      <Button onPress={signout}>Sair</Button>
    </Container>
  );
};

export default User;
