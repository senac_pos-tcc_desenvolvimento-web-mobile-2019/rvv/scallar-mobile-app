import styled from 'styled-components/native';

interface ViewProps {
  elevation: number;
  incrementSize?: number;
}

export const Scroll = styled.ScrollView``;

export const CardGroup = styled.View`
  display: flex;
  align-items: center;
  margin-bottom: 16px;
`;

export const RowView = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const TiecktCard = styled.View<ViewProps>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 325px;
  margin: 12px;
  padding: 16px;
  height: ${({incrementSize}) =>
    incrementSize ? 225 + incrementSize * 18 : 225}px;
  border-radius: 25px;
  background-color: #12967c;
`;

export const ChartCard = styled.View<ViewProps>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 325px;
  margin: 12px;
  padding: 16px;
  height: 175px;
  border-radius: 25px;
  background-color: #12967c;
`;

export const CardBody = styled.View`
  padding-left: 8px;
`;

export const CardLabel = styled.Text`
  color: #cceee9;
  font-size: 18px;
`;

export const StrongLabel = styled.Text`
  font-weight: bold;
  font-size: 24px;
`;

export const CardValue = styled.Text`
  color: #a5e1d8;
  overflow: hidden;
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 8px;
`;

export const CardNumericValue = styled.Text`
  color: #a5e1d8;
  overflow: hidden;
  max-width: 150px;
  font-size: 36px;
  font-weight: bold;
  margin-bottom: 8px;
`;

export const CardFooter = styled.View`
  align-items: center;
`;

export const Title = styled.Text`
  font-weight: bold;
  font-size: 24px;
  margin: 8px 0;
`;

export const FormGroup = styled.View`
  justify-content: center;
  align-items: center;
  margin-bottom: 24px;
`;
