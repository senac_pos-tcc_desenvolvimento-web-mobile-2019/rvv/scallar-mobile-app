import React, {useCallback, useMemo, useRef, useEffect, useState} from 'react';
import {View, Dimensions} from 'react-native';
import {Form} from '@unform/mobile';
import {FormHandles} from '@unform/core';
import Months from '../../../utils/getMonths';
import DropDown, {DropdownDataProps} from '../../../components/Dropdown';
import Button from '../../../components/Button';
import {LineChart} from 'react-native-chart-kit';
import {
  Scroll,
  RowView,
  FormGroup,
  CardGroup,
  TiecktCard,
  ChartCard,
  CardBody,
  CardLabel,
  StrongLabel,
  CardValue,
  CardNumericValue,
} from './styles';
import api from '../../../services/api';

const Romi: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const [mes, setMes] = useState('');
  const [romi, setRomi] = useState(0);
  const [validaRomi, setValidaRomi] = useState(0);
  const [dataChart, setDataChart] = useState<number[]>();
  const screenWidth = Dimensions.get('window').width;
  const [cardIncrementSize, setCardIncrementSize] = useState(0);

  useEffect(() => {
    async function load() {
      let loadDataChart = [];
      const response = await api.get('/paginaRomiAPI');
      setRomi(response.data.romi_pagina);
      setValidaRomi(response.data.valida_romi);
      if (response.data.valida_romi.length > 40) {
        setCardIncrementSize((response.data.valida_romi.length - 40) / 20);
      }
      for (let i = 0; i < 12; i++) {
        loadDataChart[i] = response.data[`romi_${Months[i].name}`];
      }
      setDataChart(loadDataChart);
    }
    load();
  }, []);

  const handleSubmit = useCallback(async (data) => {
    let loadDataChart = [];
    if (data.ano_id === 0) {
      data.ano_id = 2020;
    }
    if (data.periodo_id === 0) {
      data.periodo_id = 1;
    }
    const response = await api.post('/pesquisaRomi', data);
    setMes(Months[data.periodo_id - 1].label);
    setRomi(response.data.romi_pagina);
    setValidaRomi(response.data.valida_romi);
    for (let i = 0; i < 12; i++) {
      loadDataChart[i] = response.data[`romi_${Months[i].name}`];
    }
    setDataChart(loadDataChart);
  }, []);

  const years = useMemo(() => {
    const now = new Date();
    const yearsList: Array<DropdownDataProps> = [];
    for (let i = now.getFullYear(); i > now.getFullYear() - 5; i--) {
      yearsList.push({label: i.toString(), value: i});
    }
    return yearsList;
  }, []);

  const chartConfig = useMemo(() => {
    return {
      backgroundGradientFrom: '#fff',
      backgroundGradientTo: '#fff',
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      barPercentage: 0.5,
    };
  }, []);

  const data = useMemo(() => {
    return {
      labels: Months.map((month) => month.la),
      datasets: [
        {
          data: dataChart || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(64, 64, 64, ${opacity})`, // optional
          strokeWidth: 5,
        },
      ],
      legend: ['ROMI'], // optional
    };
  }, [dataChart]);

  return (
    <Scroll showsVerticalScrollIndicator={false}>
      <CardGroup>
        <TiecktCard elevation={10} incrementSize={cardIncrementSize}>
          <CardBody>
            <View>
              <CardLabel>
                <StrongLabel>ROMI</StrongLabel>
                {mes ? ` de ${mes}` : ' atual '}:
              </CardLabel>
              <CardNumericValue>{romi + ' x'}</CardNumericValue>
            </View>
            <View>
              <CardLabel>Resumo: </CardLabel>
              <CardValue>{validaRomi}</CardValue>
            </View>
          </CardBody>
        </TiecktCard>
        <ChartCard elevation={10}>
          <Form ref={formRef} onSubmit={handleSubmit}>
            <FormGroup>
              <RowView>
                <DropDown name="periodo_id" data={Months} halfSize />
                <DropDown name="ano_id" data={years} halfSize />
              </RowView>
              <Button
                onPress={() => formRef.current?.submitForm()}
                color="#0c6154">
                Pesquisar
              </Button>
            </FormGroup>
          </Form>
        </ChartCard>
      </CardGroup>
      <LineChart
        data={data}
        width={screenWidth}
        height={275}
        chartConfig={chartConfig}
      />
    </Scroll>
  );
};

export default Romi;
