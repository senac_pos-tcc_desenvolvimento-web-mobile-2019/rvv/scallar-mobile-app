import React, {useState, useCallback, useRef} from 'react';
import {TextInput, Alert} from 'react-native';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import CheckBox from '@react-native-community/checkbox';
import api from '../../services/api';
import {useNavigation} from '@react-navigation/native';
import {Form} from '@unform/mobile';
import {FormHandles} from '@unform/core';
import * as Yup from 'yup';
import getErrros from '../../utils/getValidationErrors';
import Input from '../../components/Input';
import DropDown from '../../components/Dropdown';
import Button from '../../components/Button';
import {
  Container,
  Main,
  Title,
  RegisterButton,
  TextButton,
  CheckBoxContainer,
  CheckBoxLabel,
  Link,
  LinkText,
} from './styles';
import getRoles from '../../utils/getRoles';
import {TermoDeUso} from '../../docs/TermoDeUso';
import {PoliticaDePrivacidade} from '../../docs/PoliticaDePrivacidade';

interface RegisterFormData {
  nome: string;
  sobrenome: string;
  email: string;
  senha: string;
  cargo_id: string;
  fone: string;
}

const Register: React.FC = () => {
  const navigation = useNavigation();
  const [isAgree, setIsAgree] = useState(false);
  const formRef = useRef<FormHandles>(null);
  const lastNameRef = useRef<TextInput>(null);
  const emailRef = useRef<TextInput>(null);
  const passwordRef = useRef<TextInput>(null);
  const foneRef = useRef<TextInput>(null);

  const handleSubmit = useCallback(
    async (data: RegisterFormData) => {
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          nome: Yup.string().required('Nome obrigatório'),
          sobrenome: Yup.string().required('Sobrenome obrigatório'),
          email: Yup.string().required('E-mail obrigatório').email(),
          password: Yup.string().required('Senha obrigatória'),
          cargo_id: Yup.number().required('Função obrigatório'),
          fone: Yup.string(),
        });
        await schema.validate(data, {abortEarly: false});
        await api.post('/cadastrarUsuarioAPI', {
          ...data,
          cargo_id: data.cargo_id + 1,
        });
        Alert.alert(
          'Cadastro realizado com sucesso',
          'Você já pode começar a usar o aplicativo',
        );
        navigation.goBack();
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getErrros(err);
          formRef.current?.setErrors(errors);
          Alert.alert(
            'Erro ao se cadatrar',
            'Ocorreu um erro ao cadastrar seus dados, favor tentar novamente',
          );
        }
      }
    },
    [navigation],
  );

  return (
    <Container>
      <KeyboardAwareScrollView enableOnAndroid={true}>
        <Main>
          <Title>Bem-Vindo à scallar</Title>
          <Form ref={formRef} onSubmit={handleSubmit}>
            <Input
              name="nome"
              icon="user"
              autoCapitalize="words"
              placeholder="Nome"
              returnKeyType="next"
              onSubmitEditing={() => lastNameRef.current?.focus()}
            />
            <Input
              ref={lastNameRef}
              name="sobrenome"
              icon="user"
              autoCapitalize="words"
              placeholder="Sobrenome"
              returnKeyType="next"
              onSubmitEditing={() => emailRef.current?.focus()}
            />
            <Input
              ref={emailRef}
              name="email"
              icon="mail"
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              placeholder="E-mail"
              returnKeyType="next"
              onSubmitEditing={() => passwordRef.current?.focus()}
            />
            <Input
              ref={passwordRef}
              name="password"
              icon="lock"
              secureTextEntry
              textContentType="newPassword"
              placeholder="Senha"
              returnKeyType="next"
              onSubmitEditing={() => foneRef.current?.focus()}
            />
            <Input
              ref={foneRef}
              name="fone"
              icon="phone"
              autoCapitalize="none"
              placeholder="Telefone"
              returnKeyType="next"
              onSubmitEditing={() =>
                isAgree
                  ? formRef.current?.submitForm()
                  : Alert.alert('Você precisa concordar com os termos')
              }
            />
            <DropDown name="cargo_id" data={getRoles} />
            <CheckBoxContainer>
              <CheckBox
                value={isAgree}
                onValueChange={(value) => {
                  setIsAgree(value);
                }}
              />
              <CheckBoxLabel>
                Li e aceito os{' '}
                <Link
                  onPress={() => {
                    Alert.alert('Termo de Uso', TermoDeUso);
                  }}>
                  <LinkText>termos de uso</LinkText>
                </Link>{' '}
                e{' '}
                <Link
                  onPress={() => {
                    Alert.alert(
                      'Politica de Privacidade',
                      PoliticaDePrivacidade,
                    );
                  }}>
                  <LinkText>politica de privacidade</LinkText>
                </Link>
              </CheckBoxLabel>
            </CheckBoxContainer>
          </Form>
          <Button
            disabled={!isAgree}
            onPress={() => formRef.current?.submitForm()}>
            Cadastrar
          </Button>
        </Main>
      </KeyboardAwareScrollView>
      <RegisterButton onPress={() => navigation.navigate('Login')}>
        <TextButton>Voltar</TextButton>
      </RegisterButton>
    </Container>
  );
};

export default Register;
