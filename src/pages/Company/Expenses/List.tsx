import React, {useState, useEffect, useCallback} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useAuth} from '../../../hooks/Auth';
import Button from './../../../components/Button';
import {Alert, View} from 'react-native';
import CircularButton from '../../../components/CircularButton';
import {
  Container,
  Title,
  List,
  Card,
  CardBody,
  RowView,
  CardLabel,
  CardValue,
  CardFooter,
} from './styles';
import {Expense} from './dtos';
import api from '../../../services/api';
import {PageWithParams} from '../dtos';

const ListExpenses: React.FC<PageWithParams> = ({route}) => {
  const {goBack} = useNavigation();
  const [expenses, setExpenses] = useState<Expense[]>();
  const {user} = useAuth();

  useEffect(() => {
    async function load() {
      try {
        const responselist = await api.get('/listarDespesaAPI');
        setExpenses(responselist.data.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados da lista.');
      }
    }
    load();
  }, [route]);

  const handleDelete = useCallback(
    async (exp) => {
      try {
        const response = await api.delete(`excluirDespesaAPI/${exp.id}`);
        Alert.alert('Sucesso', response.data);
        const deleteListRefresh = expenses?.filter((e) => e.id !== exp.id);
        setExpenses(deleteListRefresh);
        if (exp.ano_id === new Date().getFullYear()) {
          route.params.handleDelete(exp.periodo_id);
        }
      } catch (err) {
        Alert.alert('Erros', 'Ao deletar faturamento.');
      }
    },
    [expenses, route.params],
  );

  return (
    <>
      <Container>
        <List
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={<Title>Lista de Despesas com Vendas</Title>}
          data={expenses}
          keyExtractor={(exp) => String(exp.id)}
          renderItem={({item: exp}) => {
            return (
              <Card elevation={10}>
                <CardBody>
                  <RowView>
                    <View>
                      <CardLabel>Referência: </CardLabel>
                      <CardValue>
                        {exp.periodo_id + "/" +  exp.ano_id}
                      </CardValue>
                    </View>
                    <View>
                      <CardLabel>Empresa: </CardLabel>
                      <CardValue>{user.empresa?.razao_social}</CardValue>
                    </View>
                  </RowView>
                  <CardLabel>Valor da despesa: </CardLabel>
                  <CardValue>{exp.valor.toString()}</CardValue>
                </CardBody>
                <CardFooter>
                  <RowView>
                    <Button halfSize onPress={() => goBack()}>
                      Editar
                    </Button>
                    <Button
                      halfSize
                      color="#404040"
                      onPress={() => handleDelete(exp)}>
                      Excluir
                    </Button>
                  </RowView>
                </CardFooter>
              </Card>
            );
          }}
        />
      </Container>
      <CircularButton icon="arrow-left" onPress={() => goBack()} />
    </>
  );
};

export default ListExpenses;
