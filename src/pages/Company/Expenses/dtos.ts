export interface Expense {
  id: number;
  valor: number;
  periodo_id: number;
  ano_id: number;
  empresa_id: number;
}
