export interface MarketingActions {
  id: number;
  nome: String;
  valor: number;
  tipo: string;
  tipo_investimento_id: number;
  ano_id: number;
  empresa_id: number;
  periodo_id: number;
}
