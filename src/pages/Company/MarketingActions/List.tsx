import React, {useState, useEffect, useCallback} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useAuth} from '../../../hooks/Auth';
import Button from './../../../components/Button';
import {Alert, View} from 'react-native';
import CircularButton from '../../../components/CircularButton';
import TypesInvest from '../../../utils/getTypesInvestments';
import {
  Container,
  Title,
  List,
  Card,
  CardBody,
  RowView,
  CardLabel,
  CardValue,
  CardFooter,
} from './styles';
import {MarketingActions} from './dtos';
import api from '../../../services/api';
import {PageWithParams} from '../dtos';

const ListMarketingActions: React.FC<PageWithParams> = ({route}) => {
  const {goBack} = useNavigation();
  const [marketingActions, setMarketingActions] = useState<
    MarketingActions[]
  >();
  const {user} = useAuth();

  useEffect(() => {
    async function load() {
      try {
        const responselist = await api.get('/listarAcaoAPI');
        setMarketingActions(responselist.data.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados da lista.');
      }
    }
    load();
  }, [route]);

  const handleDelete = useCallback(
    async (act) => {
      try {
        const response = await api.delete(`excluirAcaoAPI/${act.id}`);
        Alert.alert('Sucesso', response.data);
        const deleteListRefresh = marketingActions?.filter(
          (ma) => ma.id !== act.id,
        );
        setMarketingActions(deleteListRefresh);
        if (act.ano_id === new Date().getFullYear()) {
          route.params.handleDelete(act.periodo_id);
        }
      } catch (err) {
        Alert.alert('Erros', 'Ao deletar Ação.');
      }
    },
    [marketingActions, route.params],
  );

  return (
    <>
      <Container>
        <List
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={<Title>Lista de Ações de Marketing</Title>}
          data={marketingActions}
          keyExtractor={(ma) => String(ma.id)}
          renderItem={({item: ma}) => {
            if (ma.tipo.length > 10) {
              ma.tipo = ma.tipo.slice(0, 10);
            }
            return (
              <Card elevation={10}>
                <CardBody>
                  <RowView>
                    <View>
                      <CardLabel>Referência: </CardLabel>
                      <CardValue>
                        {ma.periodo_id + "/" + ma.ano_id}
                      </CardValue>
                    </View>
                    <View>
                      <CardLabel>Empresa: </CardLabel>
                      <CardValue>{user.empresa?.razao_social}</CardValue>
                    </View>
                  </RowView>
                  <RowView>
                    <View>
                      <CardLabel>Tipo: </CardLabel>
                      <CardValue>
                        {TypesInvest[ma.tipo_investimento_id - 1].la}
                      </CardValue>
                    </View>
                    <View>
                      <CardLabel>Ação: </CardLabel>
                      <CardValue>{ma.tipo}</CardValue>
                    </View>
                  </RowView>
                  <CardLabel>Valor do Faturamento: </CardLabel>
                  <CardValue>{ma.valor.toString()}</CardValue>
                </CardBody>
                <CardFooter>
                  <RowView>
                    <Button halfSize onPress={() => goBack()}>
                      Editar
                    </Button>
                    <Button
                      halfSize
                      color="#404040"
                      onPress={() => handleDelete(ma)}>
                      Excluir
                    </Button>
                  </RowView>
                </CardFooter>
              </Card>
            );
          }}
        />
      </Container>
      <CircularButton icon="arrow-left" onPress={() => goBack()} />
    </>
  );
};

export default ListMarketingActions;
