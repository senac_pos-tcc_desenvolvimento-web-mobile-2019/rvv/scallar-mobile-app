import React, {useState, useEffect, useRef, useCallback, useMemo} from 'react';
import {Form} from '@unform/mobile';
import {FormHandles} from '@unform/core';
import {useAuth} from '../../../hooks/Auth';
import * as Yup from 'yup';
import {useNavigation} from '@react-navigation/core';
import Months from '../../../utils/getMonths';
import TypesInvest from '../../../utils/getTypesInvestments';
import Input from '../../../components/Input';
import DropDown, {DropdownDataProps} from '../../../components/Dropdown';
import Button from '../../../components/Button';
import {LineChart} from 'react-native-chart-kit';
import {Alert, Dimensions, TouchableOpacity, ScrollView} from 'react-native';
import {Container, Title, FormGroup, RowView} from './styles';
import api from '../../../services/api';

interface MarketingActionsList {
  id: number;
  tipo: String;
  valor: number;
  empresa_id: number;
  periodo_id: number;
  ano_id: number;
}

const MarketingActions: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const {user} = useAuth();
  const {navigate} = useNavigation();
  const screenWidth = Dimensions.get('window').width;
  const [actions, setActions] = useState<number[]>();

  useEffect(() => {
    async function load() {
      try {
        const response = await api.get('/graficosAcoesAPI');
        setActions(response.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados do grafico.');
      }
    }
    load();
  }, []);

  const handleSubmit = useCallback(
    async (data) => {
      if (!actions) {
        return Alert.alert('Erros', 'Com os dados do grafico');
      }
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          tipo: Yup.string().required('Ação é obrigatória'),
          valor: Yup.number().required('Valor é obrigatória'),
          tipo_investimento_id: Yup.number().required('Tipo é obrigatória'),
          periodo_id: Yup.number().required('Periodo é obrigatória'),
          ano_id: Yup.number().required('Ano é obrigatória'),
          empresa_id: Yup.number().required('Empresa não foi selecionada'),
          usuario_id: Yup.number().required(
            'Usuário expirado, Logue-se novamente',
          ),
        });
        if (data.ano_id === 0) {
          data.ano_id = 2020;
        }
        if (data.periodo_id === 0) {
          data.periodo_id = 1;
        }
        if (data.tipo_investimento_id === 0) {
          data.tipo_investimento_id = 1;
        }
        data.tipo_investimento_id = Number(data.tipo_investimento_id);
        const dataToValidateAndSend = {
          ...data,
          usuario_id: user.id,
          empresa_id: user.empresa?.id,
        };
        await schema.validate(dataToValidateAndSend, {abortEarly: false});
        const responselist = await api.get('/listarAcaoAPI');
        const actionExists = responselist?.data.data.find(
          (action: MarketingActionsList) =>
            action.ano_id === data.ano_id &&
            action.periodo_id === data.periodo_id &&
            action.empresa_id === user.empresa?.id,
        );
        if (actionExists) {
          const response = await api.post(
            `/editarAcaoAPI/${actionExists.id}`,
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        } else {
          const response = await api.post(
            '/cadastrarAcaoAPI',
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        }
        const year = new Date().getFullYear();
        if (year === data.ano_id) {
          const actionUpdate = actions.map((action, idx) => {
            if (idx === data.periodo_id - 1) {
              return data.valor;
            } else {
              return action;
            }
          });
          setActions(actionUpdate);
        }
      } catch (err) {
        // todo: exception here
      }
    },
    [user, actions],
  );

  const handleDelete = useCallback(
    async (periodo_id) => {
      const dataChartRefreshed = actions?.map((fund, idx) => {
        if (idx === periodo_id - 1) {
          return 0;
        } else {
          return fund;
        }
      });
      setActions(dataChartRefreshed);
    },
    [actions],
  );

  const years = useMemo(() => {
    const now = new Date();
    const yearsList: Array<DropdownDataProps> = [];
    for (let i = now.getFullYear(); i > now.getFullYear() - 5; i--) {
      yearsList.push({label: i.toString(), value: i});
    }
    return yearsList;
  }, []);

  const data = useMemo(() => {
    return {
      labels: Months.map((month) => month.la),
      datasets: [
        {
          data: actions || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(64, 64, 64, ${opacity})`, // optional
          strokeWidth: 5,
        },
      ],
      legend: ['Ações de Marketing'], // optional
    };
  }, [actions]);

  const chartConfig = useMemo(() => {
    return {
      backgroundGradientFrom: '#fff',
      backgroundGradientTo: '#fff',
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      barPercentage: 0.5,
    };
  }, []);

  return (
    <ScrollView>
      <Container>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <FormGroup>
            <Title>Cadastro de Ações de Marketing</Title>
            <Input name="tipo" placeholder="Ação" />
            <RowView>
              <Input
                name="valor"
                keyboardType="numeric"
                placeholder="Valor"
                halfSize
              />
              <DropDown
                name="tipo_investimento_id"
                data={TypesInvest}
                halfSize
              />
            </RowView>
            <RowView>
              <DropDown name="periodo_id" data={Months} halfSize />
              <DropDown name="ano_id" data={years} halfSize />
            </RowView>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </FormGroup>
        </Form>

        <TouchableOpacity
          onPress={() =>
            navigate('CompanyListMarketingActions', {handleDelete})
          }>
          <LineChart
            data={data}
            width={screenWidth}
            height={225}
            chartConfig={chartConfig}
          />
        </TouchableOpacity>
      </Container>
    </ScrollView>
  );
};

export default MarketingActions;
