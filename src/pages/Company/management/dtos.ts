export interface RegisterFormData {
  id: string | number | undefined;
  cnpj: string;
  razao_social: string;
  numero_funcionarios: string;
  faturamento_anual: string;
  media_retencao_clientes: string;
  segmento_id: number;
}
