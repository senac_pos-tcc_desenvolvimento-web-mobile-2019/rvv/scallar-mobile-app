import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: space-between;
  margin-top: 8px;
`;

export const AvoidKeyboard = styled.KeyboardAvoidingView`
  flex: 1;
  margin-top: 16px;
`;

export const Main = styled.View`
  align-items: center;
`;

export const Title = styled.Text`
  font-weight: bold;
  font-size: 24px;
  margin-bottom: 16px;
`;

export const Footer = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  height: 32px;
  background-color: #671919;
`;

export const TextButton = styled.Text`
  color: #fff;
  font-size: 20px;
  font-weight: bold;
`;
