import React, {useEffect, useState, useCallback, useRef} from 'react';
import {TextInput, Alert, ScrollView} from 'react-native';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import {useAuth} from '../../../hooks/Auth';
import api from '../../../services/api';
import {useNavigation} from '@react-navigation/native';
import {Form} from '@unform/mobile';
import {FormHandles} from '@unform/core';
import * as Yup from 'yup';
import getErrros from '../../../utils/getValidationErrors';
import Input from '../../../components/Input';
import Button from '../../../components/Button';
import Dropdown, {DropdownDataProps} from '../../../components/Dropdown';
import {RegisterFormData} from './dtos';
import {Container, Main, Title, Footer, TextButton} from './styles';

const Register: React.FC = () => {
  const navigation = useNavigation();
  const {user, updateUser} = useAuth();
  const [company, setCompany] = useState<RegisterFormData | undefined>();
  const [segments, setSegments] = useState<DropdownDataProps>();
  const [toggleDeleteButton, setToggleDeleteButton] = useState(false);
  const formRef = useRef<FormHandles>(null);
  const companyName = useRef<TextInput>(null);
  const employees = useRef<TextInput>(null);
  const revenues = useRef<TextInput>(null);
  const retention = useRef<TextInput>(null);
  const segment = useRef<TextInput>(null);

  useEffect(() => {
    async function load() {
      try {
        const resCompany = await api.get('/listarEmpresaAPI');
        // stringfy element is necessary because forwardRef on inputs
        if (resCompany.data.data[0]) {
          const stringfyAtt = {
            ...resCompany.data.data[0],
            numero_funcionarios: resCompany.data.data[0].numero_funcionarios.toString(),
            faturamento_anual: resCompany.data.data[0].faturamento_anual.toString(),
            media_retencao_clientes: resCompany.data.data[0].media_retencao_clientes.toString(),
          };
          setCompany(stringfyAtt);
        }
        const response = await api.get('/listarSegmentoEmpresaAPI');
        const dropdownSegments = response.data.map((seg: any) => ({
          value: seg.id,
          label: seg.tipo,
        }));
        setSegments(dropdownSegments);
      } catch (err) {
        Alert.alert(
          'Empresa',
          'Cadastre uma empresa para usar a aplicação, caso ja tenha cadastrado faça login novamente',
        );
      }
    }
    load();
  }, []);

  const handleSubmit = useCallback(
    async (data: RegisterFormData) => {
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          cnpj: Yup.number().required('CPNJ é obrigatório'),
          razao_social: Yup.string().required('Rasão social é obrigatório'),
          numero_funcionarios: Yup.number().required(
            'Numero de funcionários é obrigatório',
          ),
          faturamento_anual: Yup.number().required('Faturamento é obrigatório'),
          media_retencao_clientes: Yup.string().required(
            'Retenção é obrigatório',
          ),
          segmento_id: Yup.number().required('Segmento é obrigatório'),
        });
        await schema.validate(data, {abortEarly: false});
        if (company?.id) {
          await api.post(`/editarEmpresaAPI/${company.id}`, {
            usuario_id: user.id,
            ...data,
            segmento_id: data.segmento_id,
          });
          Alert.alert('Sucesso', 'Cadastro da empresa atualizado');
        } else {
          const responseCad = await api.post('/cadastrarEmpresaAPI', {
            usuario_id: user.id,
            ...data,
            segmento_id: data.segmento_id,
          });
          // Alterar quando puder registrar mais de uma empresa
          const resCompany = await api.get('/listarEmpresaAPI');
          if (resCompany.data.data[0]) {
            user.empresa = resCompany.data.data[0];
          }
          updateUser(user);
          Alert.alert('Sucesso', responseCad.data);
        }
        navigation.navigate('Dashboard', {data});
        setToggleDeleteButton(true);
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getErrros(err);
          formRef.current?.setErrors(errors);
          Alert.alert(
            'Erro ao cadatrar empresa',
            'Favor conferir se os dados foram preenchidos corretamete',
          );
        } else {
          Alert.alert(
            'Erro ao cadatrar empresa',
            'Favor tentar novamente. \nSe o problema persistir entrar em contato',
          );
        }
      }
    },
    [company, navigation, updateUser, user],
  );

  const handleDelete = useCallback(async () => {
    if (company?.id) {
      await api.delete(`excluirEmpresaAPI/${company.id}`);
      setCompany(undefined);
      setToggleDeleteButton(false);
    }
  }, [company]);

  return (
    <ScrollView>
      <Container>
        <KeyboardAwareScrollView enableOnAndroid={true}>
          <Main>
            <Title>Registrar empresa</Title>
            <Form initialData={company} ref={formRef} onSubmit={handleSubmit}>
              <Input
                name="cnpj"
                keyboardType="numeric"
                placeholder="CNPJ"
                returnKeyType="next"
                onSubmitEditing={() => companyName.current?.focus()}
              />
              <Input
                ref={companyName}
                name="razao_social"
                autoCapitalize="words"
                placeholder="Razão social"
                returnKeyType="next"
                onSubmitEditing={() => employees.current?.focus()}
              />
              <Input
                ref={employees}
                name="numero_funcionarios"
                keyboardType="numeric"
                placeholder="Nº de funcionários"
                returnKeyType="next"
                onSubmitEditing={() => revenues.current?.focus()}
              />
              <Input
                ref={revenues}
                name="faturamento_anual"
                keyboardType="numeric"
                placeholder="Faturamento Anual"
                returnKeyType="next"
                onSubmitEditing={() => retention.current?.focus()}
              />
              <Input
                ref={retention}
                name="media_retencao_clientes"
                keyboardType="numeric"
                placeholder="Retenção de clientes mês"
                returnKeyType="next"
                onSubmitEditing={() => segment.current?.focus()}
              />
              <Dropdown name="segmento_id" data={segments} />
            </Form>
            <Button onPress={() => formRef.current?.submitForm()}>
              Cadastrar
            </Button>
          </Main>
        </KeyboardAwareScrollView>
        {toggleDeleteButton && (
          <Footer onPress={handleDelete}>
            <TextButton>Deletar</TextButton>
          </Footer>
        )}
      </Container>
    </ScrollView>
  );
};

export default Register;
