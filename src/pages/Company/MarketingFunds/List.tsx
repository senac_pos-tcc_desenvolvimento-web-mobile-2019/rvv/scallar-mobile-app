import React, {useState, useEffect, useCallback} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useAuth} from '../../../hooks/Auth';
import Button from './../../../components/Button';
import {Alert, View} from 'react-native';
import CircularButton from '../../../components/CircularButton';
import {
  Container,
  Title,
  List,
  Card,
  CardBody,
  RowView,
  CardLabel,
  CardValue,
  CardFooter,
} from './styles';
import {MarketingFunds} from './dtos';
import api from '../../../services/api';
import {PageWithParams} from '../dtos';

const ListMarketingFunds: React.FC<PageWithParams> = ({route}) => {
  const {goBack} = useNavigation();
  const [marketingFunds, setMarketingFunds] = useState<MarketingFunds[]>();
  const {user} = useAuth();

  useEffect(() => {
    async function load() {
      try {
        const responselist = await api.get('/listarVerbaAPI');
        setMarketingFunds(responselist.data.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados da lista.');
      }
    }
    load();
  }, [route]);

  const handleDelete = useCallback(
    async (mf) => {
      try {
        const response = await api.delete(`excluirVerbaAPI/${mf.id}`);
        Alert.alert('Sucesso', response.data);
        const deleteListRefresh = marketingFunds?.filter((m) => m.id !== mf.id);
        setMarketingFunds(deleteListRefresh);
        if (mf.ano_id === new Date().getFullYear()) {
          route.params.handleDelete(mf.periodo_id);
        }
      } catch (err) {
        Alert.alert('Erros', 'Ao deletar verba.');
      }
    },
    [marketingFunds, route.params],
  );

  return (
    <>
      <Container>
        <List
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={<Title>Lista de Verba de Marketing</Title>}
          data={marketingFunds}
          keyExtractor={(mf) => String(mf.id)}
          renderItem={({item: mf}) => {
            return (
              <Card elevation={10}>
                <CardBody>
                  <RowView>
                    <View>
                      <CardLabel>Referência: </CardLabel>
                      <CardValue>
                        {mf.periodo_id + "/" + mf.ano_id}
                      </CardValue>
                    </View>
                    <View>
                      <CardLabel>Empresa: </CardLabel>
                      <CardValue>{user.empresa?.razao_social}</CardValue>
                    </View>
                  </RowView>
                  <CardLabel>Verba de marketing: </CardLabel>
                  <CardValue>{mf.verba.toString()}</CardValue>
                </CardBody>
                <CardFooter>
                  <RowView>
                    <Button halfSize onPress={() => goBack()}>
                      Editar
                    </Button>
                    <Button
                      halfSize
                      color="#404040"
                      onPress={() => handleDelete(mf)}>
                      Excluir
                    </Button>
                  </RowView>
                </CardFooter>
              </Card>
            );
          }}
        />
      </Container>
      <CircularButton icon="arrow-left" onPress={() => goBack()} />
    </>
  );
};

export default ListMarketingFunds;
