import React, {useState, useEffect, useRef, useCallback, useMemo} from 'react';
import {Form} from '@unform/mobile';
import {useNavigation} from '@react-navigation/core';
import {FormHandles} from '@unform/core';
import {useAuth} from '../../../hooks/Auth';
import * as Yup from 'yup';
import Months from '../../../utils/getMonths';
import Input from '../../../components/Input';
import DropDown, {DropdownDataProps} from '../../../components/Dropdown';
import Button from '../../../components/Button';
import {LineChart} from 'react-native-chart-kit';
import {Alert, Dimensions, TouchableOpacity, ScrollView} from 'react-native';
import {Container, Title, FormGroup, RowView} from './styles';
import api from '../../../services/api';

interface MarketingFundsList {
  id: number;
  verba: number;
  empresa_id: number;
  periodo_id: number;
  ano_id: number;
}

const MarketingFunds: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const {user} = useAuth();
  const {navigate} = useNavigation();
  const screenWidth = Dimensions.get('window').width;
  const [funds, setFunds] = useState<number[]>();

  useEffect(() => {
    async function load() {
      try {
        const response = await api.get('/graficosVerbaAPI');
        setFunds(response.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados do grafico.');
      }
    }
    load();
  }, []);

  const handleSubmit = useCallback(
    async (data) => {
      if (!funds) {
        return Alert.alert('Erros', 'Com os dados do grafico');
      }
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          verba: Yup.number().required('Verba é obrigatória'),
          periodo_id: Yup.number().required('Periodo é obrigatória'),
          ano_id: Yup.number().required('Ano é obrigatória'),
          empresa_id: Yup.number().required('Empresa não foi selecionada'),
          usuario_id: Yup.number().required(
            'Usuário expirado, Logue-se novamente',
          ),
        });
        if (data.ano_id === 0) {
          data.ano_id = 2020;
        }
        if (data.periodo_id === 0) {
          data.periodo_id = 1;
        }
        data.verba = Number(data.verba);
        const dataToValidateAndSend = {
          ...data,
          usuario_id: user.id,
          empresa_id: user.empresa?.id,
        };
        await schema.validate(dataToValidateAndSend, {abortEarly: false});
        const responselist = await api.get('/listarVerbaAPI');
        const fundExists = responselist?.data.data.find(
          (fund: MarketingFundsList) =>
            fund.ano_id === data.ano_id &&
            fund.periodo_id === data.periodo_id &&
            fund.empresa_id === user.empresa?.id,
        );
        if (fundExists) {
          const response = await api.post(
            `/editarVerbaAPI/${fundExists.id}`,
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        } else {
          const response = await api.post(
            '/cadastrarVerbaAPI',
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        }
        const year = new Date().getFullYear();
        if (year === data.ano_id) {
          const fundsUpdate = funds.map((fund, idx) => {
            if (idx === data.periodo_id - 1) {
              return data.verba;
            } else {
              return fund;
            }
          });
          setFunds(fundsUpdate);
        }
      } catch (err) {
        // todo: exception here
      }
    },
    [user, funds],
  );

  const handleDelete = useCallback(
    async (periodo_id) => {
      const dataChartRefreshed = funds?.map((fund, idx) => {
        if (idx === periodo_id - 1) {
          return 0;
        } else {
          return fund;
        }
      });
      setFunds(dataChartRefreshed);
    },
    [funds],
  );

  const years = useMemo(() => {
    const now = new Date();
    const yearsList: Array<DropdownDataProps> = [];
    for (let i = now.getFullYear(); i > now.getFullYear() - 5; i--) {
      yearsList.push({label: i.toString(), value: i});
    }
    return yearsList;
  }, []);

  const data = useMemo(() => {
    return {
      labels: Months.map((month) => month.la),
      datasets: [
        {
          data: funds || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(64, 64, 64, ${opacity})`, // optional
          strokeWidth: 5,
        },
      ],
      legend: ['Verba'], // optional
    };
  }, [funds]);

  const chartConfig = useMemo(() => {
    return {
      backgroundGradientFrom: '#fff',
      backgroundGradientTo: '#fff',
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      barPercentage: 0.5,
    };
  }, []);

  return (
    <ScrollView>
      <Container>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <FormGroup>
            <Title>Cadastro de Verba de Marketing</Title>
            <Input
              name="verba"
              keyboardType="numeric"
              placeholder="Verba de marketing"
            />
            <RowView>
              <DropDown name="periodo_id" data={Months} halfSize />
              <DropDown name="ano_id" data={years} halfSize />
            </RowView>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </FormGroup>
        </Form>

        <TouchableOpacity
          onPress={() => navigate('CompanyListMarketingFunds', {handleDelete})}>
          <LineChart
            data={data}
            width={screenWidth}
            height={275}
            chartConfig={chartConfig}
          />
        </TouchableOpacity>
      </Container>
    </ScrollView>
  );
};

export default MarketingFunds;
