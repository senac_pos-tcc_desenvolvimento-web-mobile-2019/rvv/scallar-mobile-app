export interface MarketingFunds {
  id: number;
  verba: number;
  ano_id: number;
  empresa_id: number;
  periodo_id: number;
}
