import React, {useRef, useCallback, useMemo, useEffect, useState} from 'react';
import {Form} from '@unform/mobile';
import {useNavigation} from '@react-navigation/core';
import {FormHandles} from '@unform/core';
import {useAuth} from '../../../hooks/Auth';
import * as Yup from 'yup';
import Months from '../../../utils/getMonths';
import Input from '../../../components/Input';
import DropDown, {DropdownDataProps} from '../../../components/Dropdown';
import Button from '../../../components/Button';
import {LineChart} from 'react-native-chart-kit';
import {Alert, Dimensions, TouchableOpacity, ScrollView} from 'react-native';
import {Container, Title, FormGroup, RowView} from './styles';
import {Investment} from './dtos';
import api from '../../../services/api';

const Investments: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const {user} = useAuth();
  const {navigate} = useNavigation();
  const screenWidth = Dimensions.get('window').width;
  const [investments, setInvestments] = useState<number[]>();

  useEffect(() => {
    async function load() {
      try {
        const response = await api.get('/graficosInvestimentosTotaisAPI');
        setInvestments(response.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados do grafico.');
      }
    }
    load();
  }, []);

  const handleSubmit = useCallback(
    async (data) => {
      if (!investments) {
        return Alert.alert('Erros', 'Com os dados do grafico');
      }
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          nome: Yup.string().required('Nome é obrigatória'),
          valor: Yup.number().required('Valor é obrigatória'),
          periodo_id: Yup.number().required('Periodo é obrigatória'),
          ano_id: Yup.number().required('Ano é obrigatória'),
          empresa_id: Yup.number().required('Empresa não foi selecionada'),
          usuario_id: Yup.number().required(
            'Usuário expirado, Logue-se novamente',
          ),
        });
        if (data.ano_id === 0) {
          data.ano_id = 2020;
        }
        if (data.periodo_id === 0) {
          data.periodo_id = 1;
        }
        data.valor = Number(data.valor);
        const dataToValidateAndSend = {
          ...data,
          usuario_id: user.id,
          empresa_id: user.empresa?.id,
        };
        await schema.validate(dataToValidateAndSend, {abortEarly: false});
        const responselist = await api.get('/listarInvestimentoTotalAPI');
        const investExists = responselist?.data.data.find(
          (invest: Investment) =>
            invest.ano_id === data.ano_id &&
            invest.periodo_id === data.periodo_id &&
            invest.empresa_id === user.empresa?.id,
        );
        if (investExists) {
          const response = await api.post(
            `/editarInvestimentoTotalAPI/${investExists.id}`,
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        } else {
          const response = await api.post(
            '/cadastrarInvestimentoTotalAPI',
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        }
        const year = new Date().getFullYear();
        if (year === data.ano_id) {
          const investmentsUpdate = investments.map((cust, idx) => {
            if (idx === data.periodo_id - 1) {
              return data.valor;
            } else {
              return cust;
            }
          });
          setInvestments(investmentsUpdate);
        }
      } catch (err) {
        // todo: exception here
      }
    },
    [user, investments],
  );

  const handleDelete = useCallback(
    async (periodo_id) => {
      const dataChartRefreshed = investments?.map((fund, idx) => {
        if (idx === periodo_id - 1) {
          return 0;
        } else {
          return fund;
        }
      });
      setInvestments(dataChartRefreshed);
    },
    [investments],
  );

  const years = useMemo(() => {
    const now = new Date();
    const yearsList: Array<DropdownDataProps> = [];
    for (let i = now.getFullYear(); i > now.getFullYear() - 5; i--) {
      yearsList.push({label: i.toString(), value: i});
    }
    return yearsList;
  }, []);

  const data = useMemo(() => {
    return {
      labels: Months.map((month) => month.la),
      datasets: [
        {
          data: investments || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(64, 64, 64, ${opacity})`, // optional
          strokeWidth: 5,
        },
      ],
      legend: ['Investimentos'], // optional
    };
  }, [investments]);

  const chartConfig = useMemo(() => {
    return {
      backgroundGradientFrom: '#fff',
      backgroundGradientTo: '#fff',
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      barPercentage: 0.5,
    };
  }, []);

  return (
    <ScrollView>
      <Container>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <FormGroup>
            <Title>Cadastro de Investimentos</Title>
            <RowView>
              <Input name="nome" placeholder="Nome" halfSize />
              <Input
                name="valor"
                keyboardType="numeric"
                placeholder="Valor"
                halfSize
              />
            </RowView>
            <RowView>
              <DropDown name="periodo_id" data={Months} halfSize />
              <DropDown name="ano_id" data={years} halfSize />
            </RowView>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </FormGroup>
        </Form>

        <TouchableOpacity
          onPress={() => navigate('CompanyListInvestments', {handleDelete})}>
          <LineChart
            data={data}
            width={screenWidth}
            height={275}
            chartConfig={chartConfig}
          />
        </TouchableOpacity>
      </Container>
    </ScrollView>
  );
};

export default Investments;
