import React, {useState, useEffect, useCallback} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useAuth} from '../../../hooks/Auth';
import Button from './../../../components/Button';
import {Alert, View} from 'react-native';
import CircularButton from '../../../components/CircularButton';
import {
  Container,
  Title,
  List,
  Card,
  CardBody,
  RowView,
  CardLabel,
  CardValue,
  CardFooter,
} from './styles';
import {Investment} from './dtos';
import api from '../../../services/api';
import {PageWithParams} from '../dtos';

const ListInvestments: React.FC<PageWithParams> = ({route}) => {
  const {goBack} = useNavigation();
  const [investments, setInvestments] = useState<Investment[]>();
  const {user} = useAuth();

  useEffect(() => {
    async function load() {
      try {
        const responselist = await api.get('/listarInvestimentoTotalAPI');
        setInvestments(responselist.data.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados do grafico.');
      }
    }
    load();
  }, [route]);

  const handleDelete = useCallback(
    async (inv) => {
      try {
        const response = await api.delete(
          `excluirInvestimentoTotalAPI/${inv.id}`,
        );
        Alert.alert('Sucesso', response.data);
        const deleteListRefresh = investments?.filter((i) => i.id !== inv.id);
        setInvestments(deleteListRefresh);
        if (inv.ano_id === new Date().getFullYear()) {
          route.params.handleDelete(inv.periodo_id);
        }
      } catch (err) {
        Alert.alert('Erros', 'Ao deletar investimento.');
      }
    },
    [investments, route.params],
  );

  return (
    <>
      <Container>
        <List
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={<Title>Lista de Investimentos</Title>}
          data={investments}
          keyExtractor={(inv) => String(inv.id)}
          renderItem={({item: inv}) => {
            return (
              <Card elevation={10}>
                <CardBody>
                  <RowView>
                    <View>
                      <CardLabel>Referência: </CardLabel>
                      <CardValue>
                        {inv.periodo_id + "/" +  inv.ano_id}
                      </CardValue>
                    </View>
                    <View>
                      <CardLabel>Empresa: </CardLabel>
                      <CardValue>{user.empresa?.razao_social}</CardValue>
                    </View>
                  </RowView>
                  <CardLabel>Nome: </CardLabel>
                  <CardValue>{inv.nome}</CardValue>
                  <CardLabel>Valor: </CardLabel>
                  <CardValue>{inv.valor.toString()}</CardValue>
                </CardBody>
                <CardFooter>
                  <RowView>
                    <Button halfSize onPress={() => goBack()}>
                      Editar
                    </Button>
                    <Button
                      halfSize
                      color="#404040"
                      onPress={() => handleDelete(inv)}>
                      Excluir
                    </Button>
                  </RowView>
                </CardFooter>
              </Card>
            );
          }}
        />
      </Container>
      <CircularButton icon="arrow-left" onPress={() => goBack()} />
    </>
  );
};

export default ListInvestments;
