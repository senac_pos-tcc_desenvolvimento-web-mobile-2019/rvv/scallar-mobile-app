export interface Investment {
  id: number;
  valor: number;
  nome: String;
  empresa_id: number;
  periodo_id: number;
  ano_id: number;
}
