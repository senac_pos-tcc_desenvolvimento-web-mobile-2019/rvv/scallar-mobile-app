import React, {useState, useCallback, useEffect} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useAuth} from '../../../hooks/Auth';
import Button from './../../../components/Button';
import {Alert, View, StyleSheet} from 'react-native';
import CircularButton from '../../../components/CircularButton';
import {
  Container,
  Title,
  List,
  Card,
  CardBody,
  RowView,
  CardLabel,
  CardValue,
  CardFooter,
} from './styles';
import {Customer} from './dtos';
import api from '../../../services/api';
import {PageWithParams} from '../dtos';

const ListCustomers: React.FC<PageWithParams> = ({route}) => {
  const {goBack} = useNavigation();
  const [customers, setCustomers] = useState<Customer[]>();
  const {user} = useAuth();

  useEffect(() => {
    async function load() {
      try {
        const responselist = await api.get('/listarClienteAPI');
        setCustomers(responselist.data.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados da lista.');
      }
    }
    load();
  }, [route]);

  const handleDelete = useCallback(
    async (cus) => {
      try {
        const response = await api.delete(`excluirClienteAPI/${cus.id}`);
        Alert.alert('Sucesso', response.data);
        const deleteListRefresh = customers?.filter((c) => c.id !== cus.id);
        setCustomers(deleteListRefresh);
        if (cus.ano_id === new Date().getFullYear()) {
          route.params.handleDelete(cus.periodo_id);
        }
      } catch (err) {
        Alert.alert('Erros', 'Ao deletar faturamento.');
      }
    },
    [customers, route.params],
  );

  const listContentStyle = StyleSheet.create({
    content: {
      alignItems: 'center',
    },
  });

  return (
    <>
      <Container>
        <List
          contentContainerStyle={listContentStyle.content}
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={<Title>Lista de Clientes</Title>}
          data={customers}
          keyExtractor={(cus) => String(cus.id)}
          renderItem={({item: cus}) => {
            return (
              <Card elevation={10}>
                <CardBody>
                  <RowView>
                    <View>
                      <CardLabel>Referência: </CardLabel>
                      <CardValue>
                        {cus.periodo_id + "/" + cus.ano_id}
                      </CardValue>
                    </View>
                    <View>
                      <CardLabel>Empresa: </CardLabel>
                      <CardValue>{user.empresa?.razao_social}</CardValue>
                    </View>
                  </RowView>
                  <CardLabel>Clientes ativos: </CardLabel>
                  <CardValue>{cus.numero_clientes.toString()}</CardValue>
                  <CardLabel>Novos clientes: </CardLabel>
                  <CardValue>{cus.novos_clientes.toString()}</CardValue>
                </CardBody>
                <CardFooter>
                  <RowView>
                    <Button halfSize onPress={() => goBack()}>
                      Editar
                    </Button>
                    <Button
                      halfSize
                      color="#404040"
                      onPress={() => handleDelete(cus)}>
                      Excluir
                    </Button>
                  </RowView>
                </CardFooter>
              </Card>
            );
          }}
        />
      </Container>
      <CircularButton icon="arrow-left" onPress={() => goBack()} />
    </>
  );
};

export default ListCustomers;
