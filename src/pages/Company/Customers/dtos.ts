export interface Customer {
  id: number;
  clientes_ativos: number;
  numero_clientes: number;
  novos_clientes: number;
  periodo_id: number;
  ano_id: number;
  empresa_id: number;
}
