import React, {useRef, useCallback, useMemo, useEffect, useState} from 'react';
import {Form} from '@unform/mobile';
import {useNavigation} from '@react-navigation/core';
import {FormHandles} from '@unform/core';
import {useAuth} from '../../../hooks/Auth';
import * as Yup from 'yup';
import Months from '../../../utils/getMonths';
import Input from '../../../components/Input';
import DropDown, {DropdownDataProps} from '../../../components/Dropdown';
import Button from '../../../components/Button';
import {LineChart} from 'react-native-chart-kit';
import {Alert, Dimensions, TouchableOpacity, ScrollView} from 'react-native';
import {Container, Title, FormGroup, RowView} from './styles';
import api from '../../../services/api';

interface Customer {
  id: number;
  empresa_id: number;
  periodo_id: number;
  ano_id: number;
}

const Customers: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const {user} = useAuth();
  const {navigate} = useNavigation();
  const screenWidth = Dimensions.get('window').width;
  const [customers, setCustomers] = useState<Customer[]>();
  const [customersDataChart, setCustomersDataChart] = useState<number[]>();

  useEffect(() => {
    async function load() {
      try {
        const responsechart = await api.get('/graficosClienteAPI');
        setCustomersDataChart(responsechart.data);
        const responselist = await api.get('/listarClienteAPI');
        setCustomers(responselist?.data.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados do grafico.');
      }
    }
    load();
  }, []);

  const handleSubmit = useCallback(
    async (data) => {
      if (!customersDataChart) {
        return Alert.alert('Erros', 'Com os dados do grafico');
      }
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          numero_clientes: Yup.number().required('Clientes é obrigatória'),
          novos_clientes: Yup.number().required('Novos clientes é obrigatória'),
          periodo_id: Yup.number().required('Periodo é obrigatória'),
          ano_id: Yup.number().required('Ano é obrigatória'),
          empresa_id: Yup.number().required('Empresa não foi selecionada'),
          usuario_id: Yup.number().required(
            'Usuário expirado, Logue-se novamente',
          ),
        });
        if (data.ano_id === 0) {
          data.ano_id = 2020;
        }
        if (data.periodo_id === 0) {
          data.periodo_id = 1;
        }
        data.numero_clientes = Number(data.numero_clientes);
        data.novos_clientes = Number(data.novos_clientes);
        const dataToValidateAndSend = {
          ...data,
          usuario_id: user.id,
          empresa_id: user.empresa?.id,
        };
        await schema.validate(dataToValidateAndSend, {abortEarly: false});
        const fundExists = customers?.find(
          (fund: Customer) =>
            fund.ano_id === data.ano_id &&
            fund.periodo_id === data.periodo_id &&
            fund.empresa_id === user.empresa?.id,
        );
        if (fundExists) {
          const response = await api.post(
            `/editarClienteAPI/${fundExists.id}`,
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        } else {
          const response = await api.post(
            '/cadastrarClienteAPI',
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        }
        const year = new Date().getFullYear();
        if (year === data.ano_id) {
          const customersDataChartUpdate = customersDataChart.map(
            (cust, idx) => {
              if (idx === data.periodo_id - 1) {
                return data.numero_clientes;
              } else {
                return cust;
              }
            },
          );
          setCustomersDataChart(customersDataChartUpdate);
        }
      } catch (err) {}
    },
    [customersDataChart, user.id, user.empresa, customers],
  );

  const handleDelete = useCallback(
    async (periodo_id) => {
      const dataChartRefreshed = customersDataChart?.map((fund, idx) => {
        if (idx === periodo_id - 1) {
          return 0;
        } else {
          return fund;
        }
      });
      setCustomersDataChart(dataChartRefreshed);
    },
    [customersDataChart],
  );

  const years = useMemo(() => {
    const now = new Date();
    const yearsList: Array<DropdownDataProps> = [];
    for (let i = now.getFullYear(); i > now.getFullYear() - 5; i--) {
      yearsList.push({label: i.toString(), value: i});
    }
    return yearsList;
  }, []);

  const data = useMemo(() => {
    return {
      labels: Months.map((month) => month.la),
      datasets: [
        {
          data: customersDataChart || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(64, 64, 64, ${opacity})`, // optional
          strokeWidth: 5,
        },
      ],
      legend: ['Clientes'], // optional
    };
  }, [customersDataChart]);

  const chartConfig = useMemo(() => {
    return {
      backgroundGradientFrom: '#fff',
      backgroundGradientTo: '#fff',
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      barPercentage: 0.5,
    };
  }, []);

  return (
    <ScrollView>
      <Container>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <FormGroup>
            <Title>Cadastro de Clientes</Title>
            <RowView>
              <Input
                name="numero_clientes"
                keyboardType="numeric"
                placeholder="Ativos"
                halfSize
              />
              <Input
                name="novos_clientes"
                keyboardType="numeric"
                placeholder="Novos"
                halfSize
              />
            </RowView>
            <RowView>
              <DropDown name="periodo_id" data={Months} halfSize />
              <DropDown name="ano_id" data={years} halfSize />
            </RowView>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </FormGroup>
        </Form>

        <TouchableOpacity
          onPress={() => navigate('CompanyListCustomers', {handleDelete})}>
          <LineChart
            data={data}
            width={screenWidth}
            height={275}
            chartConfig={chartConfig}
          />
        </TouchableOpacity>
      </Container>
    </ScrollView>
  );
};

export default Customers;
