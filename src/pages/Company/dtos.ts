export interface PageWithParams {
  route: {
    params: {
      handleDelete(periodo_id: number): void;
      data?: any;
    };
  };
}
