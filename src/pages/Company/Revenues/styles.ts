import styled from 'styled-components/native';
import {FlatList} from 'react-native';
import {Revenue} from './dtos';

interface ViewProps {
  elevation: number;
}

export const Container = styled.View`
  justify-content: space-between;
  align-items: center;
`;

export const Title = styled.Text`
  font-weight: bold;
  font-size: 24px;
  margin: 8px 0;
`;

export const FormGroup = styled.View`
  justify-content: center;
  align-items: center;
  margin-bottom: 24px;
`;

export const RowView = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const List = styled(FlatList as new () => FlatList<Revenue>)`
  margin-bottom: 8px;
`;

export const Card = styled.View<ViewProps>`
  margin: 12px;
  padding: 32px;
  width: 325px;
  height: 275px;
  border-radius: 25px;
  background-color: #fff;
`;

export const CardBody = styled.View``;

export const CardLabel = styled.Text`
  font-size: 16px;
`;

export const CardValue = styled.Text`
  overflow: hidden;
  max-width: 100px;
  max-height: 35px;
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const CardFooter = styled.View`
  align-items: center;
`;
