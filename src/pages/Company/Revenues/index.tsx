import React, {useRef, useCallback, useMemo, useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/core';
import {Form} from '@unform/mobile';
import {FormHandles} from '@unform/core';
import {useAuth} from '../../../hooks/Auth';
import * as Yup from 'yup';
import Months from '../../../utils/getMonths';
import Input from '../../../components/Input';
import DropDown, {DropdownDataProps} from '../../../components/Dropdown';
import Button from '../../../components/Button';
import {LineChart} from 'react-native-chart-kit';
import {Alert, Dimensions, TouchableOpacity, ScrollView} from 'react-native';
import {Container, Title, FormGroup, RowView} from './styles';
import api from '../../../services/api';

interface RevenuesList {
  id: number;
  empresa_id: number;
  periodo_id: number;
  ano_id: number;
}

const Revenues: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const {user} = useAuth();
  const {navigate} = useNavigation();
  const [revenues, setRevenues] = useState<number[]>();
  const screenWidth = Dimensions.get('window').width;

  useEffect(() => {
    async function load() {
      console.log();
      try {
        const response = await api.get('/graficosFaturamentoAPI');
        setRevenues(response.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados do grafico.');
      }
    }
    load();
  }, []);

  const handleSubmit = useCallback(
    async (data) => {
      if (!revenues) {
        return Alert.alert('Erros', 'Com os dados do grafico');
      }
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          valor_faturamento: Yup.number().required('Faturamento é obrigatória'),
          periodo_id: Yup.number().required('Periodo é obrigatória'),
          ano_id: Yup.number().required('Ano é obrigatória'),
          empresa_id: Yup.number().required('Empresa não foi selecionada'),
          usuario_id: Yup.number().required(
            'Usuário expirado, Logue-se novamente',
          ),
        });
        if (data.ano_id === 0) {
          data.ano_id = 2020;
        }
        if (data.periodo_id === 0) {
          data.periodo_id = 1;
        }
        data.valor_faturamento = Number(data.valor_faturamento);
        const dataToValidateAndSend = {
          ...data,
          usuario_id: user.id,
          empresa_id: user.empresa?.id,
        };
        await schema.validate(dataToValidateAndSend, {abortEarly: false});
        const responselist = await api.get('/listarFaturamentoAPI');
        const revenueExists = responselist?.data.data.find(
          (revenue: RevenuesList) =>
            revenue.ano_id === data.ano_id &&
            revenue.periodo_id === data.periodo_id &&
            revenue.empresa_id === user.empresa?.id,
        );
        if (revenueExists) {
          const response = await api.post(
            `/editarFaturamentoAPI/${revenueExists.id}`,
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        } else {
          const response = await api.post(
            '/cadastrarFaturamentoAPI',
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        }
        const year = new Date().getFullYear();
        if (year === data.ano_id) {
          const revenuesUpdate = revenues.map((fund, idx) => {
            if (idx === data.periodo_id - 1) {
              return data.valor_faturamento;
            } else {
              return fund;
            }
          });
          setRevenues(revenuesUpdate);
        }
      } catch (err) {
        // todo: exception here
      }
    },
    [revenues, user.empresa, user.id],
  );

  const handleDelete = useCallback(
    async (periodo_id) => {
      const revenuesRefreshed = revenues?.map((fund, idx) => {
        if (idx === periodo_id - 1) {
          return 0;
        } else {
          return fund;
        }
      });
      setRevenues(revenuesRefreshed);
    },
    [revenues],
  );

  const years = useMemo(() => {
    const now = new Date();
    const yearsList: Array<DropdownDataProps> = [];
    for (let i = now.getFullYear(); i > now.getFullYear() - 5; i--) {
      yearsList.push({label: i.toString(), value: i});
    }
    return yearsList;
  }, []);

  const data = useMemo(() => {
    return {
      labels: Months.map((month) => month.la),
      datasets: [
        {
          data: revenues || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(18, 150, 124, ${opacity})`, // optional
          strokeWidth: 5,
        },
      ],
      legend: ['Faturamento'], // optional
    };
  }, [revenues]);

  const chartConfig = useMemo(() => {
    return {
      backgroundGradientFrom: '#fff',
      backgroundGradientTo: '#fff',
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      barPercentage: 0.5,
    };
  }, []);

  return (
    <ScrollView>
      <Container>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <FormGroup>
            <Title>Cadastro de Faturamentos</Title>
            <Input
              name="valor_faturamento"
              keyboardType="numeric"
              placeholder="Valor do Faturamento"
            />
            <RowView>
              <DropDown name="periodo_id" data={Months} halfSize />
              <DropDown name="ano_id" data={years} halfSize />
            </RowView>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </FormGroup>
        </Form>
        <TouchableOpacity
          onPress={() => navigate('CompanyListRevenues', {handleDelete})}>
          <LineChart
            data={data}
            width={screenWidth}
            height={275}
            chartConfig={chartConfig}
          />
        </TouchableOpacity>
      </Container>
    </ScrollView>
  );
};

export default Revenues;
