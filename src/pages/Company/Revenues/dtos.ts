export interface Revenue {
  id: number;
  valor_faturamento: number;
  periodo_id: number;
  ano_id: number;
  empresa_id: number;
}
