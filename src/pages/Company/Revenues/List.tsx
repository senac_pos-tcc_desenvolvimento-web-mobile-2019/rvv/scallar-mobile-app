import React, {useState, useEffect, useCallback} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useAuth} from '../../../hooks/Auth';
import Button from './../../../components/Button';
import {Alert, View} from 'react-native';
import CircularButton from '../../../components/CircularButton';
import {
  Container,
  Title,
  List,
  Card,
  CardBody,
  RowView,
  CardLabel,
  CardValue,
  CardFooter,
} from './styles';
import {Revenue} from './dtos';
import api from '../../../services/api';
import {PageWithParams} from '../dtos';

const ListRevenues: React.FC<PageWithParams> = ({route}) => {
  const {goBack} = useNavigation();
  const [revenues, setRevenues] = useState<Revenue[]>();
  const {user} = useAuth();

  useEffect(() => {
    async function load() {
      try {
        const responselist = await api.get('/listarFaturamentoAPI');
        setRevenues(responselist.data.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados da lista.');
      }
    }
    load();
  }, [route]);

  const handleDelete = useCallback(
    async (rev) => {
      try {
        const response = await api.delete(`excluirFaturamentoAPI/${rev.id}`);
        Alert.alert('Sucesso', response.data);
        const deleteListRefresh = revenues?.filter((r) => r.id !== rev.id);
        setRevenues(deleteListRefresh);
        if (rev.ano_id === new Date().getFullYear()) {
          route.params.handleDelete(rev.periodo_id);
        }
      } catch (err) {
        Alert.alert('Erros', 'Ao deletar faturamento.');
      }
    },
    [revenues, route.params],
  );
  return (
    <>
      <Container>
        <List
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={<Title>Lista de Faturamentos</Title>}
          data={revenues}
          keyExtractor={(rev) => String(rev.id)}
          renderItem={({item: rev}) => {
            return (
              <Card elevation={10}>
                <CardBody>
                  <RowView>
                    <View>
                      <CardLabel>Referência: </CardLabel>
                      <CardValue>
                        {rev.periodo_id + "/" + rev.ano_id}
                      </CardValue>
                    </View>
                    <View>
                      <CardLabel>Empresa: </CardLabel>
                      <CardValue>{user.empresa?.razao_social}</CardValue>
                    </View>
                  </RowView>
                  <CardLabel>Valor do Faturamento: </CardLabel>
                  <CardValue>{rev.valor_faturamento.toString()}</CardValue>
                </CardBody>
                <CardFooter>
                  <RowView>
                    <Button halfSize onPress={() => goBack()}>
                      Editar
                    </Button>
                    <Button
                      halfSize
                      color="#404040"
                      onPress={() => handleDelete(rev)}>
                      Excluir
                    </Button>
                  </RowView>
                </CardFooter>
              </Card>
            );
          }}
        />
      </Container>
      <CircularButton icon="arrow-left" onPress={() => goBack()} />
    </>
  );
};

export default ListRevenues;
