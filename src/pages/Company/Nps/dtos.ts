export interface Nps {
  id: number;
  promotores: number;
  detratores: number;
  neutros: number;
  periodo_id: number;
  empresa_id: number;
  ano_id: number;
}
