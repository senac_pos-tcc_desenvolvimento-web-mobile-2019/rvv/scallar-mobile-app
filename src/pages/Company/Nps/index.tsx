import React, {useState, useEffect, useRef, useCallback, useMemo} from 'react';
import {Form} from '@unform/mobile';
import {useNavigation} from '@react-navigation/core';
import {FormHandles} from '@unform/core';
import {useAuth} from '../../../hooks/Auth';
import * as Yup from 'yup';
import Months from '../../../utils/getMonths';
import Input from '../../../components/Input';
import DropDown, {DropdownDataProps} from '../../../components/Dropdown';
import Button from '../../../components/Button';
import {LineChart} from 'react-native-chart-kit';
import {Alert, Dimensions, TouchableOpacity, ScrollView} from 'react-native';
import {Container, Title, FormGroup, RowView} from './styles';
import {Nps as INps} from './dtos';
import api from '../../../services/api';

const Nps: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const {user} = useAuth();
  const {navigate} = useNavigation();
  const screenWidth = Dimensions.get('window').width;
  const [nps, setNps] = useState<number[]>();
  const [promoters, setPromoters] = useState<number[]>();
  const [neutrals, setNeutrals] = useState<number[]>();
  const [detractors, setDetractors] = useState<number[]>();

  useEffect(() => {
    async function load() {
      try {
        const response = await api.get('/graficosDadosNPSAPI');
        setPromoters(response.data.slice(0, 12));
        setDetractors(response.data.slice(12, 24));
        setNeutrals(response.data.slice(24, 36));
        setNps(response.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados do grafico.');
      }
    }
    load();
  }, []);

  const handleSubmit = useCallback(
    async (data) => {
      if (!nps || !promoters || !neutrals || !detractors) {
        return Alert.alert('Erros', 'Com os dados do grafico');
      }
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          promotores: Yup.number().required('Promotores é obrigatória'),
          neutros: Yup.number().required('Neutros é obrigatória'),
          detratores: Yup.number().required('Detratores é obrigatória'),
          periodo_id: Yup.number().required('Periodo é obrigatória'),
          ano_id: Yup.number().required('Ano é obrigatória'),
          empresa_id: Yup.number().required('Empresa não foi selecionada'),
          usuario_id: Yup.number().required(
            'Usuário expirado, Logue-se novamente',
          ),
        });
        if (data.ano_id === 0) {
          data.ano_id = 2020;
        }
        if (data.periodo_id === 0) {
          data.periodo_id = 1;
        }
        data.promotores = Number(data.promotores);
        data.neutros = Number(data.neutros);
        data.detratores = Number(data.detratores);
        const dataToValidateAndSend = {
          ...data,
          usuario_id: user.id,
          empresa_id: user.empresa?.id,
        };
        await schema.validate(dataToValidateAndSend, {abortEarly: false});
        const responselist = await api.get('/listarDadosNPSAPI');
        const npsExists = responselist?.data.data.find(
          (_nps: INps) =>
            _nps.ano_id === data.ano_id &&
            _nps.periodo_id === data.periodo_id &&
            _nps.empresa_id === user.empresa?.id,
        );
        if (npsExists) {
          const response = await api.post(
            `/editarDadosNPSAPI/${npsExists.id}`,
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        } else {
          const response = await api.post(
            '/cadastrarDadosNPSAPI',
            dataToValidateAndSend,
          );
          Alert.alert('Sucesso', response.data);
        }
        const year = new Date().getFullYear();
        if (year === data.ano_id) {
          const promotersUpdate = promoters.map((promoter, idx) => {
            if (idx === data.periodo_id - 1) {
              return data.promotores;
            } else {
              return promoter;
            }
          });
          setPromoters(promotersUpdate);
          const neutralsUpdate = neutrals.map((neutral, idx) => {
            if (idx === data.periodo_id - 1) {
              return data.neutros;
            } else {
              return neutral;
            }
          });
          setNeutrals(neutralsUpdate);
          const detractorsUpdate = detractors.map((detractor, idx) => {
            if (idx === data.periodo_id - 1) {
              return data.detratores;
            } else {
              return detractor;
            }
          });
          setDetractors(detractorsUpdate);
        }
      } catch (err) {
        // todo: exception here
      }
    },
    [nps, promoters, neutrals, detractors, user.id, user.empresa],
  );

  const handleDelete = useCallback(
    async (periodo_id) => {
      const dataChartPromotorsRefreshed = promoters?.map((fund, idx) => {
        if (idx === periodo_id - 1) {
          return 0;
        } else {
          return fund;
        }
      });
      const dataChartNeutralsRefreshed = neutrals?.map((fund, idx) => {
        if (idx === periodo_id - 1) {
          return 0;
        } else {
          return fund;
        }
      });
      const dataChartDetractorsRefreshed = detractors?.map((fund, idx) => {
        if (idx === periodo_id - 1) {
          return 0;
        } else {
          return fund;
        }
      });
      setPromoters(dataChartPromotorsRefreshed);
      setNeutrals(dataChartNeutralsRefreshed);
      setDetractors(dataChartDetractorsRefreshed);
    },
    [detractors, neutrals, promoters],
  );

  const years = useMemo(() => {
    const now = new Date();
    const yearsList: Array<DropdownDataProps> = [];
    for (let i = now.getFullYear(); i > now.getFullYear() - 5; i--) {
      yearsList.push({label: i.toString(), value: i});
    }
    return yearsList;
  }, []);

  const data = useMemo(() => {
    return {
      labels: Months.map((month) => month.la),
      datasets: [
        {
          data: promoters || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(18, 150, 124, ${opacity})`, // optional
          strokeWidth: 5,
        },
        {
          data: neutrals || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(64, 64, 64, ${opacity})`, // optional
          strokeWidth: 5,
        },
        {
          data: detractors || [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
          color: (opacity = 1) => `rgba(197, 48, 48, ${opacity})`, // optional
          strokeWidth: 5,
        },
      ],
      legend: ['Promotores', 'Neutros', 'Detratores'], // optional
    };
  }, [detractors, neutrals, promoters]);

  const chartConfig = useMemo(() => {
    return {
      backgroundGradientFrom: '#fff',
      backgroundGradientTo: '#fff',
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      barPercentage: 0.5,
    };
  }, []);

  return (
    <ScrollView>
      <Container>
        <Form ref={formRef} onSubmit={handleSubmit}>
          <FormGroup>
            <Title>NPS</Title>
            <Input
              name="promotores"
              keyboardType="numeric"
              placeholder="Promotor"
            />
            <RowView>
              <Input
                name="neutros"
                keyboardType="numeric"
                placeholder="Neutro"
                halfSize
              />
              <Input
                name="detratores"
                keyboardType="numeric"
                placeholder="Detrator"
                halfSize
              />
            </RowView>
            <RowView>
              <DropDown name="periodo_id" data={Months} halfSize />
              <DropDown name="ano_id" data={years} halfSize />
            </RowView>
            <Button onPress={() => formRef.current?.submitForm()}>
              Salvar
            </Button>
          </FormGroup>
        </Form>

        <TouchableOpacity
          onPress={() => navigate('CompanyListNps', {handleDelete})}>
          <LineChart
            data={data}
            width={screenWidth}
            height={225}
            chartConfig={chartConfig}
          />
        </TouchableOpacity>
      </Container>
    </ScrollView>
  );
};

export default Nps;
