import React, {useState, useEffect, useCallback} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useAuth} from '../../../hooks/Auth';
import Button from './../../../components/Button';
import {Alert, View} from 'react-native';
import CircularButton from '../../../components/CircularButton';
import {
  Container,
  Title,
  List,
  Card,
  CardBody,
  RowView,
  CardLabel,
  CardValue,
  CardFooter,
} from './styles';
import {Nps} from './dtos';
import api from '../../../services/api';
import {PageWithParams} from '../dtos';

const ListNps: React.FC<PageWithParams> = ({route}) => {
  const {goBack} = useNavigation();
  const [nps, setNps] = useState<Nps[]>();
  const {user} = useAuth();

  useEffect(() => {
    async function load() {
      try {
        const responselist = await api.get('/listarDadosNPSAPI');
        setNps(responselist.data.data);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados da lista.');
      }
    }
    load();
  }, [route]);

  const handleDelete = useCallback(
    async (spn) => {
      try {
        const response = await api.delete(`excluirDadosNPSAPI/${spn.id}`);
        Alert.alert('Sucesso', response.data);
        const deleteListRefresh = nps?.filter((p) => p.id !== spn.id);
        setNps(deleteListRefresh);
        if (spn.ano_id === new Date().getFullYear()) {
          route.params.handleDelete(spn.periodo_id);
        }
      } catch (err) {
        Alert.alert('Erros', 'Ao deletar faturamento.');
      }
    },
    [nps, route.params],
  );

  return (
    <>
      <Container>
        <List
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={<Title>Dados do NPS</Title>}
          data={nps}
          keyExtractor={(np) => String(np.id)}
          renderItem={({item: np}) => {
            return (
              <Card elevation={10}>
                <CardBody>
                  <RowView>
                    <View>
                      <CardLabel>Referência: </CardLabel>
                      <CardValue>
                        {np.periodo_id + "/" +  np.ano_id}
                      </CardValue>
                    </View>
                    <View>
                      <CardLabel>Empresa: </CardLabel>
                      <CardValue>{user.empresa?.razao_social}</CardValue>
                    </View>
                  </RowView>
                  <CardLabel>Numero de promotores: </CardLabel>
                  <CardValue>{np.promotores.toString()}</CardValue>
                  <CardLabel>Numero de neutros: </CardLabel>
                  <CardValue>{np.neutros.toString()}</CardValue>
                  <CardLabel>Numero detratores: </CardLabel>
                  <CardValue>{np.detratores.toString()}</CardValue>
                </CardBody>
                <CardFooter>
                  <RowView>
                    <Button halfSize onPress={() => goBack()}>
                      Editar
                    </Button>
                    <Button
                      halfSize
                      color="#404040"
                      onPress={() => handleDelete(np)}>
                      Excluir
                    </Button>
                  </RowView>
                </CardFooter>
              </Card>
            );
          }}
        />
      </Container>
      <CircularButton icon="arrow-left" onPress={() => goBack()} />
    </>
  );
};

export default ListNps;
