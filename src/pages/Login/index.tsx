import React, {useCallback, useRef} from 'react';
import {TextInput, Alert} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Form} from '@unform/mobile';
import {FormHandles} from '@unform/core';
import * as Yup from 'yup';
import getErrros from '../../utils/getValidationErrors';
import {useAuth} from '../../hooks/Auth';
import Logoimg from '../../assets/scallar_marca.png';
import Input from '../../components/Input';
import Button from '../../components/Button';
import {
  Container,
  AvoidKeyboard,
  Main,
  Logo,
  Title,
  RegisterButton,
  TextButton,
  ForgotPassword,
  ForgotPasswordText,
} from './styles';

interface LoginFormData {
  email: string;
  senha: string;
}

const Login: React.FC = () => {
  const navigation = useNavigation();
  const formRef = useRef<FormHandles>(null);
  const inputPasswordRef = useRef<TextInput>(null);
  const {signin} = useAuth();

  const handleSubmit = useCallback(
    async (data: LoginFormData) => {
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          email: Yup.string().required('E-mail obrigatório').email(),
          senha: Yup.string().required('Senha obrigatória'),
        });
        await schema.validate(data, {abortEarly: false});
        await signin({
          email: data.email,
          senha: data.senha,
        });
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getErrros(err);
          formRef.current?.setErrors(errors);
        }
        Alert.alert(
          'Erro ao se autenticar',
          'Ocorreu um erro ao comparar seus dados, favor conferir suas credenciais',
        );
      }
    },
    [signin],
  );

  return (
    <Container>
      <AvoidKeyboard behavior="position">
        <Main>
          <Logo source={Logoimg} />
          <Title>Bem-Vindo à scallar</Title>
          <Form ref={formRef} onSubmit={handleSubmit}>
            <Input
              name="email"
              icon="mail"
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              placeholder="E-mail"
              returnKeyType="next"
              onSubmitEditing={() => inputPasswordRef.current?.focus()}
            />
            <Input
              ref={inputPasswordRef}
              name="senha"
              icon="lock"
              secureTextEntry
              placeholder="Senha"
              returnKeyType="send"
              onSubmitEditing={() => formRef.current?.submitForm()}
            />
          </Form>
          <Button onPress={() => formRef.current?.submitForm()}>Entrar</Button>
          <ForgotPassword onPress={() => navigation.navigate('Register')}>
            <ForgotPasswordText>Esqueci minha senha</ForgotPasswordText>
          </ForgotPassword>
        </Main>
      </AvoidKeyboard>
      <RegisterButton onPress={() => navigation.navigate('Register')}>
        <TextButton>Cadastrar</TextButton>
      </RegisterButton>
    </Container>
  );
};

export default Login;
