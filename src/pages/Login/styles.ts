import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: space-between;
`;

export const AvoidKeyboard = styled.KeyboardAvoidingView`
  flex: 1;
  margin-top: 16px;
`;

export const Main = styled.View`
  align-items: center;
`;

export const Logo = styled.Image`
  position: relative;
  top: -5px;
  width: 200px;
  height: 200px;
`;

export const Title = styled.Text`
  font-weight: bold;
  font-size: 24px;
  margin-bottom: 16px;
`;

export const ForgotPassword = styled.TouchableOpacity`
  margin-top: 8px;
`;

export const ForgotPasswordText = styled.Text`
  font-size: 16px;
  color: #404040;
`;

export const RegisterButton = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  height: 48px;
  background-color: #404040;
`;

export const TextButton = styled.Text`
  color: #fff;
  font-size: 20px;
  font-weight: bold;
`;
