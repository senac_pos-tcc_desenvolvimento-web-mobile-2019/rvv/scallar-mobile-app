import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: space-between;
`;

export const AvoidKeyboard = styled.KeyboardAvoidingView`
  flex: 1;
  margin-top: 16px;
`;

export const Main = styled.View`
  align-items: center;
`;

export const Title = styled.Text`
  font-weight: bold;
  font-size: 24px;
  margin-bottom: 16px;
`;

export const LogoutButton = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  height: 48px;
  background-color: #712f2f;
`;

export const TextButton = styled.Text`
  color: #fff;
  font-size: 20px;
  font-weight: bold;
`;

export const CheckBoxContainer = styled.View`
  flex-direction: row;
  padding-left: 5px;
  width: 300px;
  margin-bottom: 4px;
  align-items: center;
`;

export const CheckBoxLabel = styled.Text`
  padding-right: 30px;
  text-align: center;
  font-size: 18px;
`;

export const Link = styled.TouchableWithoutFeedback``;

export const LinkText = styled.Text`
  margin-top: 5px;
  padding-right: 30px;
  text-align: center;
  font-size: 18px;
  color: #12967c;
  text-decoration: underline;
`;

export const ViewRow = styled.View`
  flex-direction: row;
`;
