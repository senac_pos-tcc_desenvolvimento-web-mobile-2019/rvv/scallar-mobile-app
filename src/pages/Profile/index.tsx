import React, {useState, useCallback, useRef, useEffect} from 'react';
import {TextInput, Alert} from 'react-native';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import {useAuth} from '../../hooks/Auth';
import api from '../../services/api';
import {Form} from '@unform/mobile';
import {FormHandles} from '@unform/core';
import * as Yup from 'yup';
import getErrros from '../../utils/getValidationErrors';
import Input from '../../components/Input';
import DropDown from '../../components/Dropdown';
import Button from '../../components/Button';
import CheckBox from '@react-native-community/checkbox';
import getRoles from '../../utils/getRoles';
import {
  Container,
  Main,
  Title,
  LogoutButton,
  TextButton,
  CheckBoxContainer,
  CheckBoxLabel,
} from './styles';

interface RegisterFormData {
  nome: string;
  sobrenome: string;
  email: string;
  password?: string;
  cargo_id: string;
}

const Register: React.FC = () => {
  const [changePassword, setChangePassword] = useState(false);
  const {user, updateUser, signout} = useAuth();
  const [userlog, setUserlog] = useState<RegisterFormData>();
  const formRef = useRef<FormHandles>(null);
  const lastNameRef = useRef<TextInput>(null);
  const emailRef = useRef<TextInput>(null);
  const passwordRef = useRef<TextInput>(null);

  useEffect(() => {
    async function load() {
      const response = await api.get('/informacoesUsuarioLogadoAPI');
      setUserlog(response.data);
    }
    load();
  }, [user, updateUser]);

  const handleSubmit = useCallback(
    async (data: RegisterFormData) => {
      try {
        formRef.current?.setErrors({});
        const schema = Yup.object().shape({
          nome: Yup.string().required('Nome obrigatório'),
          sobrenome: Yup.string().required('Sobrenome obrigatório'),
          email: Yup.string().required('E-mail obrigatório').email(),
          cargo_id: Yup.number().required('Função obrigatório'),
          password: Yup.string(),
        });
        await schema.validate(data, {abortEarly: false});
        if (!changePassword) {
          delete data?.password;
        }
        updateUser({id: user.id, name: data.nome, ...data});
        await api.post(`editarUsuarioLogadoAPI/${user.id}`, data);
        Alert.alert(
          'Cadastro atualizado com sucesso',
          'Para atulizar os dados na aplicação pode ser necessario logar novamente sistema',
        );
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const errors = getErrros(err);
          formRef.current?.setErrors(errors);
          Alert.alert(
            'Erro ao se cadatrar',
            'Ocorreu um erro ao cadastrar seus dados, favor tentar novamente',
          );
        }
      }
    },
    [changePassword, updateUser, user.id],
  );

  return (
    <Container>
      <KeyboardAwareScrollView enableOnAndroid={true}>
        <Main>
          <Title>Perfil</Title>
          <Form ref={formRef} onSubmit={handleSubmit} initialData={userlog}>
            <Input
              name="nome"
              icon="user"
              autoCapitalize="words"
              placeholder="Nome"
              returnKeyType="next"
              onSubmitEditing={() => lastNameRef.current?.focus()}
            />
            <Input
              ref={lastNameRef}
              name="sobrenome"
              icon="user"
              autoCapitalize="words"
              placeholder="Sobrenome"
              returnKeyType="next"
              onSubmitEditing={() => emailRef.current?.focus()}
            />
            <Input
              ref={emailRef}
              name="email"
              icon="mail"
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              placeholder="E-mail"
              returnKeyType="next"
              onSubmitEditing={() => passwordRef.current?.focus()}
            />
            <CheckBoxContainer>
              <CheckBox
                disabled={true}
                value={changePassword}
                onValueChange={(value) => {
                  setChangePassword(value);
                }}
              />
              <CheckBoxLabel>Alterar Senha (Em Construção) </CheckBoxLabel>
            </CheckBoxContainer>
            <Input
              editable={changePassword}
              ref={passwordRef}
              name="password"
              icon="lock"
              secureTextEntry
              textContentType="newPassword"
              placeholder="Senha"
              returnKeyType="next"
              onSubmitEditing={() => formRef.current?.submitForm()}
            />
            <DropDown name="cargo_id" data={getRoles} />
          </Form>
          <Button onPress={() => formRef.current?.submitForm()}>
            Atualizar
          </Button>
        </Main>
      </KeyboardAwareScrollView>
      <LogoutButton onPress={signout}>
        <TextButton>Efetuar Logout</TextButton>
      </LogoutButton>
    </Container>
  );
};

export default Register;
