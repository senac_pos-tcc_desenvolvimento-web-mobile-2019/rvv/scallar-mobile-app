import React, {useState, useEffect, useCallback} from 'react';
import {useNavigation} from '@react-navigation/core';
import Button from './../../components/Button';
import {Alert, View, ActivityIndicator} from 'react-native';
import {
  Container,
  Title,
  List,
  CardBody,
  RowView,
  CardLabel,
  CardValue,
  CardFooter,
} from './styles';
import {RegisterFormData} from './../Company/Management/dtos';
import api from './../../services/api';
import PageWithParams from '../Company/dtos';

const ListCompanies: React.FC<PageWithParams> = ({route}) => {
  const {navigate} = useNavigation();
  const [companies, setCompanies] = useState<RegisterFormData[]>();
  const [segments, setSegments] = useState<Array<{id: number; tipo: string}>>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function load() {
      try {
        setLoading(true);
        const responselist = await api.get('/listarEmpresaAPI');
        setCompanies(responselist.data.data);
        const responseSeg = await api.get('listarSegmentoEmpresaAPI');
        setSegments(responseSeg.data);
        setLoading(false);
      } catch (err) {
        Alert.alert('Erros', 'Ao carregar dados da lista.');
      }
    }
    load();
  }, [route]);

  const handleDelete = useCallback(
    async (id) => {
      try {
        const response = await api.delete(`excluirEmpresaAPI/${id}`);
        Alert.alert('Sucesso', response.data);
        const deleteListRefresh = companies?.filter((cmp) => cmp.id !== id);
        setCompanies(deleteListRefresh);
      } catch (err) {
        Alert.alert('Erros', 'Ao deletar empresa.');
      }
    },
    [companies],
  );
  return (
    <>
      <Container>
        <List
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={<Title>Empresas</Title>}
          data={companies}
          keyExtractor={(cmp) => String(cmp.id)}
          renderItem={({item: cmp}) => {
            return (
              <>
                <CardBody>
                  <View>
                    <CardLabel>Razão social: </CardLabel>
                    <CardValue>{cmp.razao_social}</CardValue>
                  </View>
                  <View>
                    <CardLabel>CNPJ: </CardLabel>
                    <CardValue>{cmp.cnpj}</CardValue>
                  </View>
                  <View>
                    <CardLabel>Segmento: </CardLabel>
                    <CardValue>
                      {segments && segments[cmp.segmento_id - 1].tipo}
                    </CardValue>
                  </View>
                  <View>
                    <CardLabel>Número de funcionários; </CardLabel>
                    <CardValue>{cmp.numero_funcionarios}</CardValue>
                  </View>
                  <View>
                    <CardLabel>Média de retenção de clientes; </CardLabel>
                    <CardValue>{cmp.media_retencao_clientes}</CardValue>
                  </View>
                  <View>
                    <CardLabel>Faturamento anual: </CardLabel>
                    <CardValue>{cmp.faturamento_anual}</CardValue>
                  </View>
                </CardBody>
                <CardFooter>
                  <RowView>
                    <Button
                      halfSize
                      onPress={() => navigate('CompanyManagement')}>
                      Editar
                    </Button>
                    <Button
                      halfSize
                      color="#404040"
                      onPress={() => handleDelete(cmp.id)}>
                      Excluir
                    </Button>
                  </RowView>
                </CardFooter>
                {loading && <ActivityIndicator size="large" color="#12967c" />}
              </>
            );
          }}
        />
      </Container>
    </>
  );
};

export default ListCompanies;
